// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,

  /***/
  api: {
    photo_url: 'https://webtest.snrshopping.com/',
    urlAdjustments: 'https://webtest.snrshopping.com/api/v1',
    url: 'https://webtest.snrshopping.com/api/v1/s',
    s_url: 'https://webtest.snrshopping.com/api/v1/s',
    p_url: 'https://webtest.snrshopping.com/api/v1/p',
    snr: 'https://snr-ecom.snrshopping.com:662'
  },

  /**
  api: {
    urlAdjustments: 'https://staging.snrshopping.com/api/v1',
    url: 'https://staging.snrshopping.com/api/v1/s',
    s_url: 'https://staging.snrshopping.com/api/v1/s',
    p_url: 'https://staging.snrshopping.com/api/v1/p',
    snr: 'https://snr-ecom.snrshopping.com:662'
  },*/


  /**
  api: {
    photo_url: 'https://www.snrshopping.com/',
    urlAdjustments: 'https://www.snrshopping.com/api/v1',
    url: 'https://www.snrshopping.com/api/v1/s',
    s_url: 'https://www.snrshopping.com/api/v1/s',
    p_url: 'https://www.snrshopping.com/api/v1/p',
    snr: 'https://snr-ecom.snrshopping.com:662'
  },*/
  type: '1'  //1 = shopper, 2 = picker
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
