import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserTrainingPageRoutingModule } from './user-training-routing.module';

import { UserTrainingPage } from './user-training.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserTrainingPageRoutingModule
  ],
  declarations: [UserTrainingPage]
})
export class UserTrainingPageModule {}
