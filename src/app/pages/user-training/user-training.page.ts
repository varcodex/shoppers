import { Component, OnInit } from '@angular/core';
import { AutoLogoutService } from '../../services/auto-logout.service';
@Component({
  selector: 'app-user-training',
  templateUrl: './user-training.page.html',
  styleUrls: ['./user-training.page.scss'],
})
export class UserTrainingPage implements OnInit {

  constructor(private autoLogout: AutoLogoutService) { }

  ngOnInit() {
  }

}
