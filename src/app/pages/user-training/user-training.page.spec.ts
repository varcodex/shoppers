import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserTrainingPage } from './user-training.page';

describe('UserTrainingPage', () => {
  let component: UserTrainingPage;
  let fixture: ComponentFixture<UserTrainingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTrainingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserTrainingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
