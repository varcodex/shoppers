import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForDeliveryOrderPageRoutingModule } from './for-delivery-order-routing.module';

import { ForDeliveryOrderPage } from './for-delivery-order.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForDeliveryOrderPageRoutingModule
  ],
  declarations: [ForDeliveryOrderPage]
})
export class ForDeliveryOrderPageModule {}
