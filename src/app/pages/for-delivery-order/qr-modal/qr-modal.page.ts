import { Component, OnInit } from '@angular/core';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import { ModalController, NavParams } from '@ionic/angular';
import { AutoLogoutService } from '../../../services/auto-logout.service';
@Component({
  selector: 'app-qr-modal',
  templateUrl: './qr-modal.page.html',
  styleUrls: ['./qr-modal.page.scss'],
})
export class QrModalPage implements OnInit {
  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  value = '';
  constructor(private modalController: ModalController,
    private navParams: NavParams,
    private autoLogout: AutoLogoutService) { }

  ngOnInit() {
    this.value = this.navParams.data.qr;
  }

  async closeModal() {
    await this.modalController.dismiss(null);
  }

}
