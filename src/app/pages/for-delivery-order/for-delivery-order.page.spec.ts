import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ForDeliveryOrderPage } from './for-delivery-order.page';

describe('ForDeliveryOrderPage', () => {
  let component: ForDeliveryOrderPage;
  let fixture: ComponentFixture<ForDeliveryOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForDeliveryOrderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ForDeliveryOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
