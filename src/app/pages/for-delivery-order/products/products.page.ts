import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../services/order.service';
import { Storage } from '@ionic/storage';
import { Product } from '../../../models/product';
import { ProductData } from '../../../models/product-data';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import { ModalController } from '@ionic/angular';
import { QrModalPage } from '../qr-modal/qr-modal.page';
import { ImageModalPage } from '../image-modal/image-modal.page';
import { AutoLogoutService } from '../../../services/auto-logout.service';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  status: any;
  segmentModel = 'order';
  token: any;
  // tslint:disable-next-line: variable-name
  shoppers_id: any;
  products: Product[] = [];
  productDatas: ProductData[] = [];
  today: any;
  total = 0;
  orderId: any;
  jobOrderNo: any;
  areaOfDelivery: any = '';
  deliveryDate: any;
  CustomerName: any;
  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  value = '';
  scannedData: any;
  encodedData: any;
  OrderStatusId: any;
  isCompleted: string;
  orderStatus: any;
  color = 'danger';
  orders: any;
  membershipNumber = '009999999909';
  dateReceived: any;
  receivedBy: any;
  deliveryLevel: any;
  deliveryDate1: any;
  deliveryDate2: any;
  cost_summary: any;
  haId: any;
  membershipStatus: any;
  hasMembership: any;
  title: any;
  gender: string;
  email: any;
  mobileNo = 'N/A';
  landlineNo = 'N/A';
  createdAt: any;
  birthdate: string;
  constructor(private orderService: OrderService,
    private storage: Storage,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public modalController: ModalController,
    private autoLogout: AutoLogoutService) {
    this.status = 'order-details';
    this.orderId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  async ngOnInit() {
    this.token = await this.getStorage('token');
    this.shoppers_id = await this.getStorage('shoppersId');
    this.orders = await this.getStorage('order');
    for (const order of this.orders) {
      this.jobOrderNo = order.job_order_number;
      this.cost_summary = order.cost_summary;
      this.hasMembership = order.has_membership;
      if (order.shipping_company_name || order.shipping_company_name == '') {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_company_name}`;
      }
      if (order.shipping_building_no || order.shipping_building_no == '') {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_building_no}`;
      }
      if (order.shipping_street_name || order.shipping_street_name == '') {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_street_name}`;
      }
      if (order.shipping_barangay || order.shipping_barangay == '') {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_barangay}`;
      }
      if (order.shipping_city || order.shipping_city == '') {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_city}`;
      }
      if (order.holding_area) {
        this.receivedBy = `${order.holding_area.employee_id}`;
      }
      this.haId = order.ha_id;
      this.deliveryDate1 = order.delivery_date_time;
      this.deliveryDate2 = order.delivery_date_time2;
      this.deliveryLevel = order.delivery_level;
      this.dateReceived = order.ha_recieved_date;
      this.deliveryDate = order.delivery_date_time;
      if (this.hasMembership) {
        this.membershipStatus = order.membership_status.membership_status;
        if (order.membership_status.membership_number != 'N/A') {
          this.membershipNumber = order.membership_status.membership_number;
        }
      }
      if (order.member) {
        this.title = order.member.title;
        if (order.member.title == 'Mr.') {
          this.gender = 'Male'
        } else if (order.member.title == 'Ms.') {
          this.gender = 'Female'
        }
        this.email = order.member.email;
        if (order.member.mobile_number != null) {
          this.mobileNo = order.member.mobile_number;
        }
        if (order.member.landline_number != null) {
          this.landlineNo = order.member.landline_number;
        }
        this.createdAt = order.member.created_at;
        this.birthdate = `${order.member.birthdate_month}/${order.member.birthdate_day}/${order.member.birthdate_year}`;
        this.createdAt = order.member.created_at;
      }
      this.CustomerName = `${order.shipping_first_name} ${order.shipping_last_name}`;
      this.OrderStatusId = order.order_status_id;
    }
    await this.getData();
    if (this.OrderStatusId == 5 && this.haId == null) {
      this.isCompleted = ``;
      this.orderStatus = ``;
      this.color = `danger`;
    } else if (this.OrderStatusId == 5 && this.haId != null) {
      this.isCompleted = `completed`;
      this.orderStatus = `For Staging`;
      this.color = `success`;
    } else if (this.OrderStatusId == 6) {
      this.isCompleted = `completed`;
      this.orderStatus = `Out For Staging`;
      this.color = `success`;
    } else if (this.OrderStatusId == 7) {
      this.isCompleted = `completed`;
      this.orderStatus = `Delivered`;
      this.color = `success`;
    }
  }

  async getData(): Promise<any> {
    // tslint:disable-next-line: prefer-const
    let result;
    try {
      const params = {
        order_id: this.orderId
      };
      await this.orderService.getOrderProducts(this.token, params).subscribe(async res => {
        if (res) {
          // tslint:disable-next-line: no-string-literal
          if (res['success']) {
            // tslint:disable-next-line: no-string-literal
            this.products.push({ success: res['success'], data: res['data'], message: res['message'], code: res['code'] });
            console.log(this.products)
            for (const product of this.products) {
              for (const data of product.data) {
                if ((data.deleted_at == null && data.is_replaced == 0) || (data.deleted_at != null && data.is_replaced == 1)) {
                  let isReplaced = 0;
                  let isOutOfStock = 0;
                  let status = 0;
                  let color = 'primary';
                  if (data.is_replaced == 1) {
                    isReplaced = 1;
                    color = 'success';
                    status = 2;
                  } else if (data.is_out_of_stock == 1) {
                    isOutOfStock = 1;
                    color = 'danger';
                    status = 3;
                  } else {
                    if (data.inventory.sku == "136") {
                      color = 'success';
                      status = 2;
                    }
                    this.total = this.total + Number(data.quantity);
                    for (let i = 1; i <= Number(data.quantity); i++) {
                      if (this.value == null) {
                        if (data.inventory.sku == "136") {
                          this.value = data.inventory.sku;
                        } else {
                          if (data.inventory.products.product_type_id == "2") {
                            if (data.sub_products[i - 1].weight != '0') {
                              this.value = data.sub_products[i - 1].scanned_upc
                            }
                          } else {
                            this.value = data.scanned_upc;
                          }
                        }
                      } else {
                        if (data.inventory.sku == "136") {
                          this.value = this.value + `\r\n` + data.inventory.sku;
                        } else {
                          if (data.inventory.products.product_type_id == "2") {
                            if (data.sub_products[i - 1].weight != '0') {
                              this.value = this.value + `\r\n` + data.sub_products[i - 1].scanned_upc
                            }
                          } else {
                            this.value = this.value + `\r\n` + data.scanned_upc;
                          }
                        }
                      }
                    }
                  }

                  //start of fresh items
                  if (data.inventory.products.product_type_id == "2") {
                    if (data.is_replaced == 1) {
                      this.productDatas.push({
                        quantity: data.quantity,
                        is_same_prod_diff_size: data.is_same_prod_diff_size,
                        is_same_size_diff_brand: data.is_same_size_diff_brand,
                        is_no_substitution: data.is_no_substitution,
                        is_out_of_stock: isOutOfStock,
                        is_dc_delivery: data.is_dc_delivery,
                        delivery_date_time: data.dc_schedule,
                        id: data.id,
                        product_id: data.product_id,
                        sku: data.inventory.sku,
                        department_id: data.inventory.department_id,
                        sku_location: data.inventory.products.sku_location,
                        name: data.inventory.products.name,
                        featured_image: this.url(data.inventory.products.featured_image),
                        product_type_id: data.inventory.products.product_type_id,
                        sub_product_id: 0,
                        upc: data.scanned_upc,
                        price_history: data.price_history,
                        weight: '1',
                        color: color,
                        isEdited: 0,
                        isReplaced: isReplaced,
                        status: status
                      });
                    } else {
                      for (let i = 1; i <= Number(data.quantity); i++) {
                        if (data.sub_products.length > 0) {
                          let weight;
                          let sub_product_id;
                          if (data.sub_products.length >= i) {
                            if (data.sub_products[i - 1].weight != '0') {
                              weight = data.sub_products[i - 1].weight;
                              sub_product_id = data.sub_products[i - 1].id;
                              color = 'success';
                              status = 1;
                              isOutOfStock = 0;
                            } else {
                              weight = data.sub_products[i - 1].weight;
                              sub_product_id = data.sub_products[i - 1].id;
                              isOutOfStock = 1;
                              color = 'success';
                              status = 3;
                              this.total = this.total - 1;
                            }
                          } else {
                            weight = '1';
                            isOutOfStock = 0;
                            sub_product_id = 0;
                            color = 'primary';
                            status = 0;
                          }
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            sub_product_id: sub_product_id,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: weight,
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                        } else {
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            sub_product_id: 0,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: '1',
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                        }

                      }
                    }
                  } else {
                    if (data.scanned_upc != null) {
                      color = 'success';
                      status = 1;
                    }
                    this.productDatas.push({
                      quantity: data.quantity,
                      is_same_prod_diff_size: data.is_same_prod_diff_size,
                      is_same_size_diff_brand: data.is_same_size_diff_brand,
                      is_no_substitution: data.is_no_substitution,
                      is_out_of_stock: isOutOfStock,
                      is_dc_delivery: data.is_dc_delivery,
                      delivery_date_time: data.dc_schedule,
                      id: data.id,
                      product_id: data.product_id,
                      sku: data.inventory.sku,
                      department_id: data.inventory.department_id,
                      sku_location: data.inventory.products.sku_location,
                      name: data.inventory.products.name,
                      featured_image: this.url(data.inventory.products.featured_image),
                      product_type_id: data.inventory.products.product_type_id,
                      sub_product_id: 0,
                      upc: '',
                      price_history: data.price_history,
                      weight: '1',
                      color: color,
                      isEdited: 0,
                      isReplaced: isReplaced,
                      status: status
                    });
                  }
                  //end of fresh items 
                }
              }
            }
            this.value = this.value.trim();
          } else {
            alert('Connection time out. Please try again or contact the administrator!');
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
      return result;
    }
    catch (e) { console.log(e); }
  }

  goToList() {
    this.router.navigate(['/menu/for-delivery-order']);
  }
  async openQRQModal() {
    const modal = await this.modalController.create({
      component: QrModalPage,
      componentProps: {
        qr: this.value
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
    });
    return await modal.present();
  }

  async doRefresh() {
    let result;
    try {
      const params = {
        shoppers_id: this.shoppers_id
      };
      await this.orderService.getAvailableOnForDeliveryOrders(this.token, params).subscribe(async res => {
        if (res) {
          if (res['success']) {
            for (let data of res['data']) {
              this.OrderStatusId = data.order_status_id;
              if (data.holding_area) {
                this.receivedBy = `${data.holding_area.employee_id}`;
              }
              if (this.OrderStatusId == 5 && data.ha_id == null) {
                this.isCompleted = ``;
                this.orderStatus = ``;
                this.color = `danger`;
              } else if (this.OrderStatusId == 5 && data.ha_id != null) {
                this.isCompleted = `completed`;
                this.orderStatus = `For Staging`;
                this.color = `success`;
              } else if (this.OrderStatusId == 6) {
                this.isCompleted = `completed`;
                this.orderStatus = `Out For Staging`;
                this.color = `success`;
              } else if (this.OrderStatusId == 7) {
                this.isCompleted = `completed`;
                this.orderStatus = `Delivered`;
                this.color = `success`;
              }
            }
          } else {
            alert('Connection time out. Please try again or contact the administrator!');
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
      return result;
    }
    catch (e) { console.log(e); }
  }

  async openImage(img) {
    const modal = await this.modalController.create({
      component: ImageModalPage,
      componentProps: {
        img
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
    });
    return await modal.present();
  }

  async getStorage(key): Promise<any> {
    try {
      const result = await this.storage.get(key);
      return result;
    }
    catch (e) { console.log(e); }
  }

  url(str) {
    const cleanUrl = JSON.parse(str);
    if (cleanUrl) {
      return environment.api.photo_url + cleanUrl[0].toString();
    }
  }

  startTime() {
    // tslint:disable-next-line: no-var-keyword
    var intervalVar = setInterval(function () {
      this.today = new Date().toISOString();
    }.bind(this), 500);
  }

}
