import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'my-account',
        loadChildren: () => import('../my-account/my-account.module').then( m => m.MyAccountPageModule)
      },
      {
        path: 'new-order',
        loadChildren: () => import('../new-order/new-order.module').then( m => m.NewOrderPageModule)
      },
      {
        path: 'process-order',
        loadChildren: () => import('../process-order/process-order.module').then( m => m.ProcessOrderPageModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('../dashboard/dashboard.module').then( m => m.DashboardPageModule)
      },
      {
        path: 'hold-order',
        loadChildren: () => import('../hold-order/hold-order.module').then( m => m.HoldOrderPageModule)
      },
      {
        path: 'cancel-order',
        loadChildren: () => import('../cancel-order/cancel-order.module').then( m => m.CancelOrderPageModule)
      },
      {
        path: 'pos-order',
        loadChildren: () => import('../pos-order/pos-order.module').then( m => m.PosOrderPageModule)
      },
      {
        path: 'for-delivery-order',
        loadChildren: () => import('../for-delivery-order/for-delivery-order.module').then( m => m.ForDeliveryOrderPageModule)
      },
      {
        path: 'user-training',
        loadChildren: () => import('../user-training/user-training.module').then( m => m.UserTrainingPageModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
