import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular'; 
import { Router, RouterEvent } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../services/auth.service';
import { DataServiceService } from '../../services/data-service.service';
import { Auth } from '../../models/auth';
import { AutoLogoutService } from '../../services/auto-logout.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  shoppersName: any;
  token: any;
  auth: Auth[] = [];
  newOrderCount: any;

  pages = [
    {
      icon: 'analytics',
      title: 'Dashboard',
      url: '/menu/dashboard'
    },
    {
      icon: 'person',
      title: 'My Account',
      url: '/menu/my-account'
    },
    {
      icon: 'albums',
      title: `New Order`,
      url: '/menu/new-order'
    },
    {
      icon: 'people',
      title: 'On Process',
      url: '/menu/process-order'
    },
    {
      icon: 'newspaper',
      title: 'For POS',
      url: '/menu/pos-order'
    },
    {
      icon: 'star',
      title: 'For Staging',
      url: '/menu/for-delivery-order'
    },
    {
      icon: 'time',
      title: 'On Hold',
      url: '/menu/hold-order'
    },
    {
      icon: 'time',
      title: 'Cancelled',
      url: '/menu/cancel-order'
    },
    {
      icon: 'folder',
      title: 'User Training',
      url: '/menu/user-training'
    }
  ];

  selectedPath = '';

  constructor(private router: Router,
              private authservice: AuthService,
              private dataServiceService: DataServiceService,
              private storage: Storage,
              private menu: MenuController,
              private autoLogout: AutoLogoutService) {
                this.dataServiceService.getToken();
                this.dataServiceService.getshoppersName();
                this.dataServiceService.getnewOrderCount();
              }

  notificationCount(title) {
    switch (title) {
        case 'New Order': return this.newOrderCount; 
    }
  }

  async ngOnInit() {
    this.router.events.subscribe(async (event: RouterEvent) => {
      if(event && event.url){
        this.selectedPath = event.url;
      }
    });
    await this.dataServiceService.castShoppersname.subscribe(data=> this.shoppersName = data);
    await this.dataServiceService.castNewOrderCount.subscribe(data=> this.newOrderCount = data);
  }

  async onlogout() {
    this.token = await this.getStorage('token');
    this.authservice.onLogout(this.token).subscribe(async res => {
      if(res['success']){
        this.menu.close();
        await this.clearStorage();
        this.router.navigate(['login']); 
      }else{
        alert('Invalid credentials/Connection time out. Please contact the administrator!');
      }  
    });
  }

  async clearStorage(): Promise<any> {
    try {
        const result =  await this.storage.clear();
        return result;
    }
    catch(e) { console.log(e) }
  }

  async getStorage(key): Promise<any> {
    try {
        const result =  await this.storage.get(key);
        return result;
    }
    catch(e) { console.log(e) }
  }

}
