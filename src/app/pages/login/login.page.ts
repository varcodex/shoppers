import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { OrderService } from '../../services/order.service';
import { DataServiceService } from '../../services/data-service.service';
import { Auth } from '../../models/auth';
import { Order } from '../../models/order';
import { Storage } from '@ionic/storage';
import { environment } from '../../../environments/environment';
import { AutoLogoutService } from '../../services/auto-logout.service';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  // tslint:disable-next-line: ban-types
  imgSrc: String = '/assets/imgs/logo.png';
  auth: Auth[] = [];
  order: Order[] = [];
  // tslint:disable-next-line: variable-name
  shoppers_id: any;
  shoppersName: any;
  appName: any;
  ionVersionNumber: string ='dev-v2.1';
  constructor(public router: Router,
              private authservice: AuthService,
              private orderService: OrderService,
              private dataServiceService: DataServiceService,
              private storage: Storage,
              private autoLogout: AutoLogoutService,
              platform: Platform,
              private appVersion: AppVersion) {
                /** 
                platform.ready().then(() => {
                  this.appVersion.getVersionNumber().then(res => {
                    this.ionVersionNumber = res;
                  }).catch(error => {
                    alert(error);
                  });
                  });
                  */
               }

  ngOnInit() {
    if(environment.type == '1'){ //shopper
     this.appName = `SHOPPER`;
    }else if(environment.type == '2'){ //picker
      this.appName = `PICKER`
    }
  }

  // tslint:disable-next-line: variable-name
  async onlogIn(employee_id, password) {
    this.storage.clear();
    this.auth = [];
    const credentials = {
      employee_id: employee_id.value,
      password: password.value
    };

    await this.authservice.onLogin(credentials).subscribe(async res => {
      if (res){
        // tslint:disable-next-line: no-string-literal
        this.auth.push({success : res['success'], data : res['data'], message : res['message']});
        for (const auth of this.auth){
          if (auth.success){
            if (Object.keys(auth.data).length > 0){
              if (Object.keys(auth.data.order).length > 0){
                // tslint:disable-next-line: triple-equals
                if (auth.data.order.order_status_id == 2){
                  await this.dataServiceService.setjobOrderNo(auth.data.order.job_order_number);
                  await this.dataServiceService.setnewOrderId(auth.data.order.id);
                  await this.dataServiceService.setshoppersName(auth.data.name);
                  await this.dataServiceService.setToken(auth.data.token);
                  await this.dataServiceService.setshoppersId(auth.data.id);
                  await this.dataServiceService.setnewOrderCount(1);
                  await this.router.navigate(['menu/dashboard']);
                }else{
                  await this.dataServiceService.setshoppersName(auth.data.name);
                  await this.dataServiceService.setToken(auth.data.token);
                  await this.dataServiceService.setshoppersId(auth.data.id);
                  await this.dataServiceService.setnewOrderCount(0);
                  await this.router.navigate(['menu/dashboard']);
                }
              }else{
                await this.dataServiceService.setshoppersName(auth.data.name);
                await this.dataServiceService.setToken(auth.data.token);
                await this.dataServiceService.setshoppersId(auth.data.id);
                await this.dataServiceService.setnewOrderCount(0);
                await this.router.navigate(['menu/dashboard']);
              }
            }
          }else{
            alert('Invalid credentials/Connection time out. Please try again or contact the administrator!');
          }
        }
      }else{
        alert('Invalid credentials/Connection time out. Please try again or contact the administrator!');
      }
    });
  }
}
