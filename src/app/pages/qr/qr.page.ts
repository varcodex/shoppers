import { Component, OnInit } from '@angular/core';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
@Component({
  selector: 'app-qr',
  templateUrl: './qr.page.html',
  styleUrls: ['./qr.page.scss'],
})
export class QrPage implements OnInit {
  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  value: any;
  constructor() { }

  ngOnInit() {
    this.value = ["17.2.0504.00002|555544XXXXXX2222|202207|KAR000SNR20221651634865|12,000.38", "17.1.0504.00005|555544XXXXXX2222|202207|KAR000SNR20221651634244|5,471.30", "17.1.0504.00006|555544XXXXXX2222|202207|KAR000SNR20221651634578|8,374.68", "17.2.0504.00001|555544XXXXXX2222|202207|KAR000SNR20221651634726|7,850.35"];
  }

}
