import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../services/order.service';
import { DataServiceService } from '../../services/data-service.service';
import { Storage } from '@ionic/storage';
import { Order } from '../../models/order';
import { Router } from '@angular/router';
import { AutoLogoutService } from '../../services/auto-logout.service';
@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.page.html',
  styleUrls: ['./new-order.page.scss'],
})
export class NewOrderPage implements OnInit {
  flag: any;
  token: any;
  new_order_id: any;
  shoppers_id: any;
  newOrderCount: any;
  order: Order[] = [];
  newOrderId: any;
  jobOrderNo: any;

  constructor(private orderService: OrderService,
    private dataServiceService: DataServiceService,
    private storage: Storage,
    private router: Router,
    private autoLogout: AutoLogoutService) {}   

  async ngOnInit() {
    this.newOrderCount = Number(await this.getStorage('newOrderCount'));
    this.token = await this.getStorage('token');
    this.shoppers_id = await this.getStorage('shoppersId');
    this.newOrderId = await this.getStorage('newOrderId');
    this.jobOrderNo = await this.getStorage('jobOrderNo');
    if(this.newOrderCount > 0){
      this.flag = true;
    }else{
      this.flag = false;
    }
  }

  async toDo(bool){
    this.token = await this.getStorage('token');
    let msg = '';
    let params = {};
    this.newOrderCount = Number(await this.getStorage('newOrderCount'));
    if(bool){
      params = {
        order_id: this.newOrderId,
        shoppers_id: this.shoppers_id,
        order_status_id: 3
      }
      msg = `You accepted the Job Order No.: ${this.jobOrderNo}`;
    }else{
      params = {
        order_id: this.newOrderId,
        notification_shopper_id: null,
        new_notification_shopper_id: this.shoppers_id
      }
      msg = `You cancelled the Job Order No.: ${this.jobOrderNo}`;
    }
    await this.orderService.updateOrder(this.token, params).subscribe(async res => {
      if(res['success']){
        this.newOrderCount = this.newOrderCount - 1;
        if(bool){
          await this.router.navigate(['menu/process-order']);
        }else{
          if(res['data']){
            await this.dataServiceService.setjobOrderNo(res['data']['job_order_number']);
            await this.dataServiceService.setnewOrderId(res['data']['id'])
            this.newOrderId = await this.getStorage('newOrderId');
            this.jobOrderNo = await this.getStorage('jobOrderNo');
            this.newOrderCount= this.newOrderCount + 1;
          }else{
            this.newOrderCount= 0;
          }
        }
        if(this.newOrderCount == 0){
          this.flag = false;
        }
        await this.dataServiceService.setnewOrderCount(this.newOrderCount);
      }else{
        alert('Connection time out. Please contact the administrator!');
      }
    });
  }

  async getStorage(key): Promise<any> {
    try {
        const result =  await this.storage.get(key);
        return result;
    }
    catch(e) { console.log(e) }
  }
}
