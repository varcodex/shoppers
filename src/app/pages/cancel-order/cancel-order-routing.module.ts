import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CancelOrderPage } from './cancel-order.page';

const routes: Routes = [
  {
    path: '',
    component: CancelOrderPage
  },
  {
    path: 'products/:id',
    loadChildren: () => import('./products/products.module').then( m => m.ProductsPageModule)
  },
  {
    path: 'image-modal',
    loadChildren: () => import('./image-modal/image-modal.module').then( m => m.ImageModalPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CancelOrderPageRoutingModule {}
