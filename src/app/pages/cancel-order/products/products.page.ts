import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrderService } from '../../../services/order.service';
import { DataServiceService } from '../../../services/data-service.service';
import { Storage } from '@ionic/storage';
import { Product } from '../../../models/product';
import { TempProduct } from '../../../models/temp-product';
import { ProductData } from '../../../models/product-data';
import { OrderStatus } from '../../../models/order-status';
import { ActivatedRoute, Router } from '@angular/router';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { ModalController } from '@ionic/angular';
import { ImageModalPage } from '../image-modal/image-modal.page';
import { AutoLogoutService } from '../../../services/auto-logout.service';
import { AlertController } from '@ionic/angular';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})

export class ProductsPage implements OnInit {
  status: any;
  segmentModel = 'order';
  token: any;
  // tslint:disable-next-line: variable-name
  shoppers_id: any;
  products: Product[] = [];
  tempProducts: TempProduct[] = [];
  productDatas: ProductData[] = [];
  orderStatus: OrderStatus[] = [];
  today: any;
  orderId: any;
  scannedData: any;
  encodedData: '';
  encodeData: any;
  dataQuantityReturned: any;
  dataProductReturned: Product[] = [];
  replaceData: ProductData[] = [];
  jobOrderNo: any;
  areaOfDelivery: any = '';
  deliveryDate: any;
  orderStatusForm: FormGroup;
  OrderStatusId: any;
  total = 0;
  CustomerName: any;
  orders: any[] = [];
  deliveryLevel: any;
  deliveryDate1: any;
  deliveryDate2: any;
  cost_summary: any;
  membershipNumber: any;
  membershipStatus: any;
  hasMembership: any;
  constructor(private barcodeScanner: BarcodeScanner,
    private orderService: OrderService,
    private storage: Storage,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dataServiceService: DataServiceService,
    public modalController: ModalController,
    private autoLogout: AutoLogoutService,
    public alertController: AlertController) {
    this.status = 'order-details';
    this.orderId = this.activatedRoute.snapshot.paramMap.get('id');
    this.orderStatusForm = this.formBuilder.group({
      orderStatus: ['', [Validators.required]],
      notes: [''],
    });
  }
  async ngOnInit() {
    this.token = await this.getStorage('token');
    this.shoppers_id = await this.getStorage('shoppersId');
    this.orders = await this.getStorage('order');

    for (const order of this.orders) {
      this.jobOrderNo = order.job_order_number;
      this.hasMembership = order.has_membership;
      this.cost_summary = order.cost_summary;
      if (order.shipping_company_name) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_company_name}`;
      }
      if (order.shipping_building_no) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_building_no}`;
      }
      if (order.shipping_street_name) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_street_name}`;
      }
      if (order.shipping_barangay) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_barangay}`;
      }
      if (order.shipping_city) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_city}`;
      }
      /** 
      if(order.member.membership_number){
        this.membershipNumber = order.member.membership_number;
        this.membershipStatus = order.member.membership_status;
      }*/
      this.deliveryDate1 = order.delivery_date_time;
      this.deliveryDate2 = order.delivery_date_time2;
      this.deliveryLevel = order.delivery_level;
      this.deliveryDate = order.delivery_date_time;
      this.CustomerName = `${order.shipping_first_name} ${order.shipping_last_name}`;
      this.orderStatusForm.get('orderStatus').setValue(`${order.order_status_id}`);
    }
    await this.getData();
  }

  async doUpdate() {
    // tslint:disable-next-line: triple-equals
    if (this.orderStatusList.value == 4) {
      let count = 0;
      for (const data of this.productDatas) {
        if (data.color == 'success') {
          count = count + 1;
        }
      }
      if (count == this.productDatas.length && this.cost_summary.subtotal != 0) {
        const capturedAmount = Number(parseFloat(this.cost_summary.total.replace(/,/g, '')).toFixed(2));
        const capture = {
          order_id: Number(this.orderId),
          captured_amount: capturedAmount
        }
        await this.orderService.capturerOrders(this.token, capture).subscribe(async res => {
          if (res['captured']) {
            const params = {
              order_id: this.orderId,
              order_status_id: this.orderStatusList.value,
              remarks: this.notes.value
            };
            await this.orderService.updateOrder(this.token, params).subscribe(async res => {
              // tslint:disable-next-line: no-string-literal
              if (res['success']) {
                // tslint:disable-next-line: no-string-literal
                alert(res['message']);
                await this.dataServiceService.setOrderStatusId(this.orderStatusList.value);
                await this.router.navigate(['menu/pos-order']);
                //await this.router.navigate(['menu/pos-order/products', this.orderId]);
                console.log('Order update successfully');
              }
            });
          } else {
            alert(res['message']);
          }
        });
      } else {
        alert(`You cannot proceed to POS, please check your items.`);
      }
    } else if (this.orderStatusList.value == 8) {
      let flag = true;
      for (const data of this.productDatas) {
        if (data.isReplaced == 0 && data.is_out_of_stock == 0) {
          flag = false
          break;
        }
      }
      if (flag) {
        alert(`Order has been placed on hold.`);
        const params = {
          order_id: this.orderId,
          order_status_id: this.orderStatusList.value,
          remarks: this.notes.value
        };
        await this.orderService.updateOrder(this.token, params).subscribe(async res => {
          if (res['success']) {
            await this.router.navigate(['menu/hold-order']);
            console.log('Order update successfully');
          }
        });
      } else {
        alert(`You cannot hold this order, please check your items.`);
      }
    } else if (this.orderStatusList.value == 9) {
      let flag = true;
      for (const data of this.productDatas) {
        if (data.isReplaced == 0 && data.is_out_of_stock == 0) {
          flag = false
          break;
        }
      }
      if (flag) {
        const params = {
          order_id: this.orderId,
          order_status_id: this.orderStatusList.value,
          remarks: this.notes.value
        };
        await this.orderService.updateOrder(this.token, params).subscribe(async res => {
          if (res['success']) {
            await this.router.navigate(['menu/cancel-order']);
            console.log('Order update successfully');
          }
        });
        alert(`Order has been canceled.`);
      } else {
        alert(`You cannot cancel this order, please check your items.`);
      }
    }
  }

  async getOrderStatusList() {
    await this.orderService.getOrderStatusList(this.token).subscribe(async res => {
      if (res) {
        // tslint:disable-next-line: no-string-literal
        if (res['success']) {
          // tslint:disable-next-line: no-string-literal
          this.orderStatus.push({ success: res['success'], data: res['data'], message: res['message'], code: res['code'] });
        }
      }
    });
  }

  async updateProductStock(index) {
    let params = {
      order_id: this.orderId,
      product_cart_id: this.productDatas[index].id,
      is_out_of_stock: 1
    }
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert!',
      message: 'Do you want to keep going?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'danger',
          handler: (blah) => {

          }
        }, {
          text: 'Yes',
          handler: async () => {
            await this.orderService.updateProductStock(this.token, params).subscribe(async res => {
              if (res['success']) {
                this.productDatas[index].status = 0;
                this.productDatas[index].is_out_of_stock = 1;
                this.productDatas[index].color = 'success';
                this.total = this.total - Number(this.productDatas[index].quantity);
                this.getCostSummary();
              }
            });
          }
        }
      ]
    });

    await alert.present();
  }

  goToList() {
    this.router.navigate(['/menu/cancel-order']);
  }

  async getData(): Promise<any> {
    // tslint:disable-next-line: prefer-const
    let result;
    try {
      const params = {
        order_id: this.orderId
      };
      await this.orderService.getOrderProducts(this.token, params).subscribe(async res => {
        if (res) {
          // tslint:disable-next-line: no-string-literal
          if (res['success']) {
            // tslint:disable-next-line: no-string-literal
            this.products.push({ success: res['success'], data: res['data'], message: res['message'], code: res['code'] });
            for (const product of this.products) {
              for (const data of product.data) {
                if ((data.deleted_at == null && data.is_replaced == 0) || (data.deleted_at != null && data.is_replaced == 1)) {
                  let isReplaced = 0;
                  let isOutOfStock = 0;
                  let status = 0;
                  let color = 'primary';
                  if (data.is_replaced == 1) {
                    isReplaced = 1;
                    color = 'success';
                    status = 2;
                  } else if (data.is_out_of_stock == 1) {
                    isOutOfStock = 1;
                    color = 'danger';
                    status = 3;
                  } else {
                    if (data.inventory.sku == "136") {
                      color = 'success';
                      status = 2;
                    }
                    this.total = this.total + Number(data.quantity);

                  }

                  //start of fresh items
                  if (data.inventory.products.product_type_id == "2") {
                    if (data.is_replaced == 1) {
                      this.productDatas.push({
                        quantity: data.quantity,
                        is_same_prod_diff_size: data.is_same_prod_diff_size,
                        is_same_size_diff_brand: data.is_same_size_diff_brand,
                        is_no_substitution: data.is_no_substitution,
                        is_out_of_stock: isOutOfStock,
                        is_dc_delivery: data.is_dc_delivery,
                        delivery_date_time: data.dc_schedule,
                        id: data.id,
                        product_id: data.product_id,
                        sku: data.inventory.sku,
                        department_id: data.inventory.department_id,
                        sku_location: data.inventory.products.sku_location,
                        name: data.inventory.products.name,
                        featured_image: this.url(data.inventory.products.featured_image),
                        product_type_id: data.inventory.products.product_type_id,
                        sub_product_id: 0,
                        upc: data.scanned_upc,
                        price_history: data.price_history,
                        weight: '1',
                        color: color,
                        isEdited: 0,
                        isReplaced: isReplaced,
                        status: status
                      });
                    } else {
                      for (let i = 1; i <= Number(data.quantity); i++) {
                        if (data.sub_products.length > 0) {
                          let weight;
                          let sub_product_id;
                          if (data.sub_products.length >= i) {
                            if (data.sub_products[i - 1].weight != '0') {
                              weight = data.sub_products[i - 1].weight;
                              sub_product_id = data.sub_products[i - 1].id;
                              color = 'success';
                              status = 1;
                              isOutOfStock = 0;
                            } else {
                              weight = data.sub_products[i - 1].weight;
                              sub_product_id = data.sub_products[i - 1].id;
                              isOutOfStock = 1;
                              color = 'success';
                              status = 3;
                              this.total = this.total - 1;
                            }
                          } else {
                            weight = '1';
                            isOutOfStock = 0;
                            sub_product_id = 0;
                            color = 'primary';
                            status = 0;
                          }
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            sub_product_id: sub_product_id,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: weight,
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                        } else {
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            sub_product_id: 0,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: '1',
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                        }
                      }
                    }
                  } else {
                    if (data.scanned_upc != null) {
                      color = 'success';
                      status = 1;
                    }
                    this.productDatas.push({
                      quantity: data.quantity,
                      is_same_prod_diff_size: data.is_same_prod_diff_size,
                      is_same_size_diff_brand: data.is_same_size_diff_brand,
                      is_no_substitution: data.is_no_substitution,
                      is_out_of_stock: isOutOfStock,
                      is_dc_delivery: data.is_dc_delivery,
                      delivery_date_time: data.dc_schedule,
                      id: data.id,
                      product_id: data.product_id,
                      sku: data.inventory.sku,
                      department_id: data.inventory.department_id,
                      sku_location: data.inventory.products.sku_location,
                      name: data.inventory.products.name,
                      featured_image: this.url(data.inventory.products.featured_image),
                      product_type_id: data.inventory.products.product_type_id,
                      sub_product_id: 0,
                      upc: '',
                      price_history: data.price_history,
                      weight: '1',
                      color: color,
                      isEdited: 0,
                      isReplaced: isReplaced,
                      status: status
                    });
                  }
                  //end of fresh items 
                }
              }
            }

          } else {
            alert('Connection time out. Please try again or contact the administrator!');
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
      return result;
    }
    catch (e) { console.log(e); }
  }

  async openImage(img) {
    const modal = await this.modalController.create({
      component: ImageModalPage,
      componentProps: {
        img
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
    });
    return await modal.present();
  }

  url(str) {
    const cleanUrl = JSON.parse(str);
    if (cleanUrl) {
      return environment.api.photo_url + cleanUrl[0].toString();
    }
  }

  async getCostSummary() {
    const params = {
      shoppers_id: this.shoppers_id
    };
    await this.orderService.getAvailableOnProcessOrders(this.token, params).subscribe(async res => {
      if (res) {
        // tslint:disable-next-line: no-string-literal
        if (res['success']) {
          // tslint:disable-next-line: no-string-literal
          for (const data of res['data']) {
            if (this.orderId == data['id']) {
              const output = JSON.parse(data['cost_summary']);
              this.cost_summary = {
                subtotal: output['subtotal'],
                concierge: output['concierge'],
                shipping_fee: output['shipping_fee'],
                non_member_surcharge: output['non_member_surcharge'],
                total: output['total']
              }
              break;
            }
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      } else {
        alert('Connection time out. Please try again or contact the administrator!');
      }
    });
  }


  async getStorage(key): Promise<any> {
    try {
      const result = await this.storage.get(key);
      return result;
    }
    catch (e) { console.log(e); }
  }

  startTime() {
    const intervalVar = setInterval(function () {
      this.today = new Date().toISOString();
    }.bind(this), 500);
  }

  get f() { return this.orderStatusForm.controls; }
  get orderStatusList() {
    return this.orderStatusForm.get('orderStatus');
  }

  get notes() {
    return this.orderStatusForm.get('notes');
  }

}
