import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HoldOrderPage } from './hold-order.page';

describe('HoldOrderPage', () => {
  let component: HoldOrderPage;
  let fixture: ComponentFixture<HoldOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoldOrderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HoldOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
