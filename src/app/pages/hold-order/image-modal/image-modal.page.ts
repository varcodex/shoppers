import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

import { AutoLogoutService } from '../../../services/auto-logout.service';
@Component({
  selector: 'app-image-modal',
  templateUrl: './image-modal.page.html',
  styleUrls: ['./image-modal.page.scss'],
})
export class ImageModalPage implements OnInit {
  img = '';
  constructor(private modalController: ModalController,
              private navParams: NavParams,
              private autoLogout: AutoLogoutService) { }

    ngOnInit() {
      this.img = this.navParams.data.img;
    }
    async closeModal() {
      await this.modalController.dismiss(null);
    }
}
