import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HoldOrderPageRoutingModule } from './hold-order-routing.module';

import { HoldOrderPage } from './hold-order.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HoldOrderPageRoutingModule
  ],
  declarations: [HoldOrderPage]
})
export class HoldOrderPageModule {}
