import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QuantityModalPageRoutingModule } from './quantity-modal-routing.module';

import { QuantityModalPage } from './quantity-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QuantityModalPageRoutingModule
  ],
  declarations: [QuantityModalPage]
})
export class QuantityModalPageModule {}
