import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-quantity-modal',
  templateUrl: './quantity-modal.page.html',
  styleUrls: ['./quantity-modal.page.scss'],
})
export class QuantityModalPage implements OnInit {

  constructor(private modalController: ModalController,
    private navParams: NavParams,) { }

  async ngOnInit() {

  }

  async closeModal() {
    await this.modalController.dismiss(null);
  }

  async doUpdate(qty) {
    if(qty){
      await this.modalController.dismiss(qty);
    }else{
      alert('Please enter valid receipt!')
    }
  }
}
