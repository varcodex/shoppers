import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PosOrderPageRoutingModule } from './pos-order-routing.module';

import { PosOrderPage } from './pos-order.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PosOrderPageRoutingModule
  ],
  declarations: [PosOrderPage]
})
export class PosOrderPageModule {}
