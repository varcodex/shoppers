import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { OrderService } from '../../services/order.service';
import { Order } from '../../models/order';
import { Product } from '../../models/product';
import { Router } from '@angular/router';
import { DataServiceService } from '../../services/data-service.service';
import { formatDate } from '@angular/common';
import { AutoLogoutService } from '../../services/auto-logout.service';
@Component({
  selector: 'app-pos-order',
  templateUrl: './pos-order.page.html',
  styleUrls: ['./pos-order.page.scss'],
})
export class PosOrderPage implements OnInit {
  token: any;
  // tslint:disable-next-line: variable-name
  shoppers_id: any;
  orders: Order[] = [];
  products: Product[] = [];
  today: any;
  deliveryDate: string;
  constructor(public router: Router,
              private orderService: OrderService,
              private dataServiceService: DataServiceService,
              private storage: Storage,
              private autoLogout: AutoLogoutService) {}

  ngOnInit() {}

  async ionViewWillEnter(){
    this.orders = [];
    this.token = await this.getStorage('token');
    this.shoppers_id = await this.getStorage('shoppersId');
    await this.getData();
  }

  async doRefresh(){
    this.orders = [];
    this.getData();
  }

  async goToPOS(id, i){
    const data: any[] = [];
    for (const order of this.orders){
      data.push({
        id: order.data[i].id,
        member_id : order.data[i].member_id,
        checkedout_date :  order.data[i].checkedout_date,
        stores_id : order.data[i].stores_id,
        job_order_number :  order.data[i].job_order_number,
        customer_service_id : order.data[i].customer_service_id,
        delivery_date_time :  order.data[i].delivery_date_time,
        delivery_date_time2 : order.data[i].delivery_date_time2,
        shipping_first_name :  order.data[i].shipping_first_name,
        shipping_last_name :  order.data[i].shipping_last_name,
        shipping_company_name :  order.data[i].shipping_company_name,
        shipping_street_name :  order.data[i].shipping_street_name,
        shipping_building_no :  order.data[i].shipping_building_no,
        shipping_phone :  order.data[i].shipping_phone,
        shipping_barangay :  order.data[i].shipping_barangay,
        shipping_city :  order.data[i].shipping_city,
        recieved_by_member : order.data[i].recieved_by_member,
        delivery_level : order.data[i].delivery_level,
        order_status_id : order.data[i].order_status_id,
        shoppers_id : order.data[i].shoppers_id,
        ccJson: JSON.parse(order.data[i].ccJson),
        has_membership: order.data[i].has_membership,
        capture_history: JSON.parse(order.data[i].capture_history),
        cost_summary :  JSON.parse(order.data[i].cost_summary),
        preauthorization_history: JSON.parse(order.data[i].preauthorization_history),
        notification_shopper_id : order.data[i].notification_shopper_id,
        membership_status: {
          customer_status: order.data[i].membership_status.customer_status,
          membership_number: order.data[i].membership_status.membership_number,
          membership_status: order.data[i].membership_status.membership_status
        },
        member: {
            email: order.data[i].member.email,
            mobile_number: order.data[i].member.mobile_number,
            landline_number: order.data[i].member.landline_number,
            created_at: order.data[i].member.created_at,
            title: order.data[i].member.title,
            birthdate_month: order.data[i].member.birthdate_month,
            birthdate_day: order.data[i].member.birthdate_day,
            birthdate_year: order.data[i].member.birthdate_year,
        }
      });
    }
    await this.dataServiceService.setOrder(data);
    await this.router.navigate(['menu/pos-order/products', id]);
  }

  async getData(): Promise<any> {
    // tslint:disable-next-line: prefer-const
    let result;
    try {
      const params = {
        shoppers_id: this.shoppers_id
      };
      await this.orderService.getAvailableOnPOSOrders(this.token, params).subscribe(async res => {

        if (res){
          // tslint:disable-next-line: no-string-literal
          console.log(res)
          if (res['success']){
            // tslint:disable-next-line: no-string-literal
            this.orders.push({success : res['success'], data : res['data'], message : res['message'], code: res['code']});
            for(let order of this.orders){
              for(let data of order.data){
                  if(data.delivery_level == '3'){
                    let d1:Date;
                    let d2:Date;
                    d1 = new Date(data.checkedout_date);
                    d2 = new Date(data.checkedout_date);
                    d1.setDate(d1.getDate() + 3);
                    d2.setDate(d2.getDate()  + 5);
                    console.log(d1)
                    data.delivery_date_time = `${formatDate(d1, 'MMMM dd', 'en-PH', '+08:00')} - ${formatDate(d2, 'dd', 'en-PH', '+08:00')}`;
                  }else{
                    data.delivery_date_time = data.delivery_date_time;
                  }
              }
            }
          }else{
            alert('Connection time out. Please try again or contact the administrator!');
          }
        }else{
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
      return result;
    }
    catch (e) { console.log(e); }
  }

  async getStorage(key): Promise<any> {
    try {
        const result =  await this.storage.get(key);
        return result;
    }
    catch (e) { console.log(e); }
  }
}
