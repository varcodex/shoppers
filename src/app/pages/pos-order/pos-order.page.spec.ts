import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PosOrderPage } from './pos-order.page';

describe('PosOrderPage', () => {
  let component: PosOrderPage;
  let fixture: ComponentFixture<PosOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosOrderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PosOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
