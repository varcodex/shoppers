import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../services/order.service';
import { DataServiceService } from '../../../services/data-service.service';
import { Storage } from '@ionic/storage';
import { Product } from '../../../models/product';
import { Order } from '../../../models/order';
import { ProductData } from '../../../models/product-data';
import { OrderStatus } from '../../../models/order-status';
import { ActivatedRoute, Router } from '@angular/router';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import { ModalController, NavController } from '@ionic/angular';
import { QrModalPage } from '../qr-modal/qr-modal.page';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { ImageModalPage } from '../image-modal/image-modal.page';
import { AutoLogoutService } from '../../../services/auto-logout.service';
import { QuantityModalPage } from '../quantity-modal/quantity-modal.page';
import { AlertController } from '@ionic/angular';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  data: Array<{ id: number, title: string, details: string, icon: string, showDetails: boolean }> = [];
  status: any;
  segmentModel = 'order';
  token: any;
  // tslint:disable-next-line: variable-name
  shoppers_id: any;
  products: Product[] = [];
  productDatas: ProductData[] = [];
  orderStatus: OrderStatus[] = [];
  newOrders: Order[] = [];
  today: any;
  total = 0;
  orderId: any;
  jobOrderNo: any;
  areaOfDelivery: any = '';
  deliveryDate: any;
  CustomerName: any;
  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  value = '';
  scannedData: any;
  encodedData: any;
  orderStatusForm: FormGroup;
  OrderStatusId: any;
  membershipNumber = '009999999909';
  qrForCashier = '';
  cardNumber = '';
  expiryDate = '';
  amount = '';
  approvalCode = '';
  orders: any[] = [];
  product: any[] = [];
  deliveryLevel: any;
  deliveryDate1: any;
  deliveryDate2: any;
  cost_summary: any;
  membershipStatus: any;
  hasMembership: any;
  account: any;
  acquirerCode: any;
  sf: any;
  dis: any;
  sur: any;
  email: any;
  mobileNo = 'N/A';
  landlineNo = 'N/A';
  createdAt: any;
  birthdate: string;
  title: any;
  gender: string;
  torDate: any;
  torData: any;
  adjustment: boolean = false;
  previousTotal: any = '';
  adjustmentTotal: any = '';
  letScan: boolean = false;
  isPayment: boolean = false;
  paymentColor: any;
  adjustColor: any;
  ccJson: any;
  maxSize: any;
  firstSize: any;
  lastSize: any;
  actualSize: any;
  curSize: number;
  preauth: any;
  preauthorization_history: any;
  showQrDetails: boolean;
  hisSubtotal: any;
  posTotal: any;
  qrOrderData: Array<{ id: number, curSize: number, actualSize: number, isChecked: boolean }> = [];
  capture_history: any;
  constructor(public navCtrl: NavController,
    private orderService: OrderService,
    private barcodeScanner: BarcodeScanner,
    private storage: Storage,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dataServiceService: DataServiceService,
    public modalController: ModalController,
    private autoLogout: AutoLogoutService,
    public alertController: AlertController) {
    this.status = 'order-details';
    this.orderId = this.activatedRoute.snapshot.paramMap.get('id');
    this.orderStatusForm = this.formBuilder.group({
      orderStatus: ['', [Validators.required]],
      notes: [''],
    });

    this.data.push({
      id: 1,
      title: 'Membership',
      details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      icon: 'arrow-forward-circle-outline',
      showDetails: false
    },
      {
        id: 2,
        title: 'Price Book',
        details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        icon: 'arrow-forward-circle-outline',
        showDetails: false
      },
      {
        id: 3,
        title: 'Order List',
        details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        icon: 'arrow-forward-circle-outline',
        showDetails: false
      },
      {
        id: 4,
        title: 'Job Order No.',
        details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        icon: 'arrow-forward-circle-outline',
        showDetails: false
      },
      {
        id: 5,
        title: 'Charges',
        details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        icon: 'arrow-forward-circle-outline',
        showDetails: false
      },
      {
        id: 6,
        title: 'Payment',
        details: '',
        icon: 'arrow-forward-circle-outline',
        showDetails: false
      });
  }

  async ngOnInit() {
    this.paymentColor = 'primary';
    this.adjustColor = 'danger';
    this.letScan = false;
    this.previousTotal = '';
    this.adjustmentTotal = '';
    this.posTotal = '';
    this.token = await this.getStorage('token');
    this.shoppers_id = await this.getStorage('shoppersId');
    this.orders = await this.getStorage('order');
    for (const order of this.orders) {
      this.jobOrderNo = order.job_order_number;
      this.cost_summary = order.cost_summary;
      if (order.capture_history) {
        this.capture_history = order.capture_history;
        console.log(this.capture_history)
        if (this.capture_history.TxnStatus == '0') {
          this.letScan = true;
          this.paymentColor = 'medium';
          this.adjustColor = 'medium';
        } else if (this.capture_history.TxnStatus == '1') {
          this.letScan = true;
          this.paymentColor = 'medium';
          this.adjustColor = 'medium';
        } else {
          this.paymentColor = 'primary';
          this.adjustColor = 'danger';
          this.letScan = false;
        }
      }
      this.preauthorization_history = order.preauthorization_history;
      this.preauth = this.preauthorization_history.total;
      this.hisSubtotal = this.cost_summary.subtotal

      if (this.cost_summary.pos_total == 0 || this.cost_summary.pos_total == '' || this.cost_summary.pos_total == null) {
        this.posTotal = this.cost_summary.total;
      } else {
        this.posTotal = this.cost_summary.pos_total;
        let total = this.cost_summary.total.split(',').join("");
        this.adjustmentTotal = Number(this.posTotal - total).toFixed(2);
      }
      console.log(this.posTotal);
      this.hasMembership = order.has_membership;
      if (order.shipping_company_name || order.shipping_company_name == '') {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_company_name}`;
      }
      if (order.shipping_building_no || order.shipping_building_no == '') {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_building_no}`;
      }
      if (order.shipping_street_name || order.shipping_street_name == '') {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_street_name}`;
      }
      if (order.shipping_barangay || order.shipping_barangay == '') {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_barangay}`;
      }
      if (order.shipping_city || order.shipping_city == '') {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_city}`;
      }
      this.deliveryDate1 = order.delivery_date_time;
      this.deliveryDate2 = order.delivery_date_time2;
      this.deliveryLevel = order.delivery_level;
      this.deliveryDate = order.delivery_date_time;
      if (this.hasMembership == 1) {
        this.membershipStatus = order.membership_status.membership_status;
        if (order.membership_status.membership_number != 'N/A') {
          this.membershipNumber = order.membership_status.membership_number;
        }
      }
      if (order.member) {
        this.title = order.member.title;
        if (order.member.title == 'Mr.') {
          this.gender = 'Male'
        } else if (order.member.title == 'Ms.') {
          this.gender = 'Female'
        }
        this.email = order.member.email;
        if (order.member.mobile_number != null) {
          this.mobileNo = order.member.mobile_number;
        }
        if (order.member.landline_number != null) {
          this.landlineNo = order.member.landline_number;
        }
        this.createdAt = order.member.created_at;
        this.birthdate = `${order.member.birthdate_month}/${order.member.birthdate_day}/${order.member.birthdate_year}`;
        this.createdAt = order.member.created_at;
      }
      if (order.ccJson) {
        this.ccJson = order.ccJson;
        console.log(this.ccJson)
        if (this.ccJson.CardNoMask == null || this.ccJson.CardNoMask == '') {

        } else {
          this.cardNumber = this.ccJson.CardNoMask.split('X').join("");
        }
        if (this.ccJson.CardExp == null || this.ccJson.CardExp == '') {

        } else {
          const year = this.ccJson.CardExp.substring(2, 4);
          const month = this.ccJson.CardExp.substring(4, 6);
          this.expiryDate = `${month}${year}`;
        }
        this.acquirerCode = this.ccJson.AcquirerCode;
        this.account = this.ccJson.Account;
        this.approvalCode = this.ccJson.AuthCode;
      }
      this.CustomerName = `${order.shipping_first_name} ${order.shipping_last_name}`;
      this.orderStatusForm.get('orderStatus').setValue(`${order.order_status_id}`);
    }

    /**
    if(Number(this.cost_summary.total) > Number(this.preauth)){
      this.isPayment = false;
      this.paymentColor = 'medium';
      console.log(this.preauth)
    }else{
      this.isPayment = true;
      this.paymentColor = 'primary';
    } */
    await this.getData();
  }

  async doUpdate() {
    if (this.orderStatusList.value == 5) {
      const params = {
        order_id: this.orderId,
        order_status_id: this.orderStatusList.value,
        remarks: this.notes.value,
        pos_transaction_number: this.encodedData,
        pos_transaction_date: formatDate(this.today, 'yyyy-MM-dd HH:mm:ss', 'en-PH', '+08:00'),
        cashier_id: this.torData,
        cc_number: this.cardNumber.split('X').join(""),
        acquirer_code: this.acquirerCode,
        approval_code: this.approvalCode,
        amount: this.cost_summary.total
      };
      console.log(params)
      await this.orderService.updateOrder(this.token, params).subscribe(async res => {
        if (res['success']) {
          if (Object.keys(res['data']).length > 0) {
            if (res['data']['order_status_id'] == 2) {
              await this.dataServiceService.setjobOrderNo(res['data']['job_order_number']);
              await this.dataServiceService.setnewOrderId(res['data']['id']);
              await this.dataServiceService.setnewOrderCount(1);
            }
          }
          await this.dataServiceService.setOrderStatusId(this.orderStatusList.value);
          await this.router.navigate(['menu/for-delivery-order']);
          console.log('Order update successfully');
        }
      });
    }
  }

  async getOrderStatusList() {
    await this.orderService.getOrderStatusList(this.token).subscribe(async res => {
      if (res) {
        // tslint:disable-next-line: no-string-literal
        if (res['success']) {
          // tslint:disable-next-line: no-string-literal
          this.orderStatus.push({ success: res['success'], data: res['data'], message: res['message'], code: res['code'] });
        }
      }
    });
  }

  async getData(): Promise<any> {
    // tslint:disable-next-line: prefer-const
    let result;
    try {
      const params = {
        order_id: this.orderId
      };
      await this.orderService.getOrderProducts(this.token, params).subscribe(async res => {
        if (res) {
          // tslint:disable-next-line: no-string-literal
          if (res['success']) {
            // tslint:disable-next-line: no-string-literal
            console.log(res);
            this.products.push({ success: res['success'], data: res['data'], message: res['message'], code: res['code'] });
            for (const product of this.products) {
              for (const data of product.data) {
                if ((data.deleted_at == null && data.is_replaced == 0) || (data.deleted_at != null && data.is_replaced == 1)) {
                  let isReplaced = 0;
                  let isOutOfStock = 0;
                  let status = 0;
                  let color = 'primary';
                  if (data.is_replaced == 1) {
                    isReplaced = 1;
                    color = 'success';
                    status = 2;
                  } else if (data.is_out_of_stock == 1) {
                    isOutOfStock = 1;
                    color = 'success';
                    status = 3;
                  } else {
                    if (data.inventory.sku == "136") {
                      color = 'success';
                      status = 2;
                    }
                    this.total = this.total + Number(data.quantity);
                    for (let i = 1; i <= Number(data.quantity); i++) {
                      if (this.product.length == 0) {
                        if (data.inventory.sku == "136") {
                          this.value = data.inventory.sku;
                          this.product.push(data.inventory.sku);
                        } else {
                          if (data.inventory.products.product_type_id == "2") {
                            if (data.sub_products.length > 0) {
                              if (data.sub_products[i - 1].weight != '0') {
                                this.value = data.sub_products[i - 1].scanned_upc;
                                this.product.push(data.sub_products[i - 1].scanned_upc);
                              }
                            }
                          } else {
                            this.value = data.scanned_upc;
                            this.product.push(data.scanned_upc);
                          }
                        }
                      } else {
                        if (data.inventory.sku == "136") {
                          this.value = this.value + `\r\n` + data.inventory.sku;
                          this.product.push(`\r\n${data.inventory.sku}`);
                        } else {
                          if (data.inventory.products.product_type_id == "2") {
                            if (data.sub_products.length > 0) {
                              if (data.sub_products[i - 1].weight != '0') {
                                this.value = this.value + `\r\n` + data.sub_products[i - 1].scanned_upc
                                this.product.push(`\r\n${data.sub_products[i - 1].scanned_upc}`);
                              }
                            }
                          } else {
                            this.value = this.value + `\r\n` + data.scanned_upc;
                            this.product.push(`\r\n${data.scanned_upc}`);
                          }
                        }
                      }
                    }
                  }

                  //start of fresh items
                  if (data.inventory.products.product_type_id == "2") {
                    if (data.is_replaced == 1) {
                      this.productDatas.push({
                        quantity: data.quantity,
                        is_same_prod_diff_size: data.is_same_prod_diff_size,
                        is_same_size_diff_brand: data.is_same_size_diff_brand,
                        is_no_substitution: data.is_no_substitution,
                        is_out_of_stock: isOutOfStock,
                        is_dc_delivery: data.is_dc_delivery,
                        delivery_date_time: data.dc_schedule,
                        id: data.id,
                        product_id: data.product_id,
                        sku: data.inventory.sku,
                        department_id: data.inventory.department_id,
                        sku_location: data.inventory.products.sku_location,
                        name: data.inventory.products.name,
                        featured_image: this.url(data.inventory.products.featured_image),
                        product_type_id: data.inventory.products.product_type_id,
                        sub_product_id: 0,
                        upc: data.scanned_upc,
                        price_history: data.price_history,
                        weight: '1',
                        color: color,
                        isEdited: 0,
                        isReplaced: isReplaced,
                        status: status
                      });
                    } else {
                      for (let i = 1; i <= Number(data.quantity); i++) {
                        if (data.sub_products.length > 0) {
                          let weight;
                          let sub_product_id;
                          if (data.sub_products.length >= i) {
                            if (data.sub_products[i - 1].weight != '0') {
                              weight = data.sub_products[i - 1].weight;
                              sub_product_id = data.sub_products[i - 1].id;
                              color = 'success';
                              status = 1;
                              isOutOfStock = 0;
                            } else {
                              weight = data.sub_products[i - 1].weight;
                              sub_product_id = data.sub_products[i - 1].id;
                              isOutOfStock = 1;
                              color = 'success';
                              status = 3;
                              this.total = this.total - 1;
                            }
                          } else {
                            weight = '1';
                            isOutOfStock = 0;
                            sub_product_id = 0;
                            color = 'primary';
                            status = 0;
                          }
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            sub_product_id: sub_product_id,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: weight,
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                        } else {
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            sub_product_id: 0,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: '1',
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                        }
                      }
                    }

                  } else {
                    if (data.scanned_upc != null) {
                      color = 'success';
                      status = 1;
                    }
                    this.productDatas.push({
                      quantity: data.quantity,
                      is_same_prod_diff_size: data.is_same_prod_diff_size,
                      is_same_size_diff_brand: data.is_same_size_diff_brand,
                      is_no_substitution: data.is_no_substitution,
                      is_out_of_stock: isOutOfStock,
                      is_dc_delivery: data.is_dc_delivery,
                      delivery_date_time: data.dc_schedule,
                      id: data.id,
                      product_id: data.product_id,
                      sku: data.inventory.sku,
                      department_id: data.inventory.department_id,
                      sku_location: data.inventory.products.sku_location,
                      name: data.inventory.products.name,
                      featured_image: this.url(data.inventory.products.featured_image),
                      product_type_id: data.inventory.products.product_type_id,
                      sub_product_id: 0,
                      upc: data.scanned_upc,
                      price_history: data.price_history,
                      weight: '1',
                      color: color,
                      isEdited: 0,
                      isReplaced: isReplaced,
                      status: status
                    });
                  }
                  //end of fresh items                
                }
              }
            }
            await this.getOrderStatusList();
            this.value = this.value.trim();
          } else {
            alert('Connection time out. Please try again or contact the administrator!');
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
      return result;
    }
    catch (e) { console.log(e); }
  }

  async openQRQModal(value) {
    const modal = await this.modalController.create({
      component: QrModalPage,
      componentProps: {
        qr: value
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
    });
    return await modal.present();
  }

  async openQROrderModal(i) {
    let value = '';
    const d = this.product.slice(i.curSize, i.actualSize)
    i.isChecked = true;
    console.log(d);
    for (let i = 0; d.length - 1 >= i; i++) {
      value = value + d[i];
    }
    console.log(value);
    const modal = await this.modalController.create({
      component: QrModalPage,
      componentProps: {
        qr: value.trim()
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
    });
    return await modal.present();
  }

  async openImage(img) {
    const modal = await this.modalController.create({
      component: ImageModalPage,
      componentProps: {
        img
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
    });
    return await modal.present();
  }

  goToList() {
    this.router.navigate(['/menu/pos-order']);
  }

  async doScan() {
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false,
      prompt: 'Place a barcode inside the scan area',
      resultDisplayDuration: 500,
      orientation: 'portrait',
    };
    this.barcodeScanner.scan(options).then(async barcodeData => {
      this.scannedData = barcodeData;
      // tslint:disable-next-line: no-string-literal
      if (this.scannedData['text'].length == 8) {
        this.encodedData = this.scannedData['text'];
        this.today = new Date().toISOString();
      } else {
        const d = this.scannedData['text'];
        alert(`Invalid receipt number ${d}`);
      }
    }).catch(err => {
      console.log('Error', err);
    });
  }

  async getCostSummary() {
    const params = {
      shoppers_id: this.shoppers_id
    };
    await this.orderService.getAvailableOnPOSOrders(this.token, params).subscribe(async res => {
      if (res) {
        // tslint:disable-next-line: no-string-literal
        if (res['success']) {
          // tslint:disable-next-line: no-string-literal
          for (const data of res['data']) {
            if (this.orderId == data['id']) {
              const output = JSON.parse(data['cost_summary']);
              const outputCCjson = JSON.parse(data['ccJson']);
              this.cost_summary = {
                subtotal: output['subtotal'],
                concierge: output['concierge'],
                shipping_fee: output['shipping_fee'],
                preauthorization_amount: output['preauthorization_amount'],
                non_member_surcharge: output['non_member_surcharge'],
                total: output['total']
              }
              this.ccJson = {
                CardNoMask: outputCCjson['CardNoMask'],
                CardHolder: outputCCjson['CardHolder'],
                CardType: outputCCjson['CardType'],
                CardExp: outputCCjson['CardExp'],
                Account: outputCCjson['Account'],
                PymtMethod: outputCCjson['PymtMethod'],
                TxnID: outputCCjson['TxnID'],
                AcquirerCode: outputCCjson['AcquirerCode'],
                AuthCode: outputCCjson['AuthCode'],
                MBAcquirerCode: outputCCjson['MBAcquirerCode'],
                Amount: outputCCjson['Amount'],
              }
              this.cardNumber = this.ccJson.CardNoMask.split('X').join("");
              const year = this.ccJson.CardExp.substring(2, 4);
              const month = this.ccJson.CardExp.substring(4, 6);
              this.expiryDate = `${month}${year}`;
              this.acquirerCode = this.ccJson.AcquirerCode;
              this.account = this.ccJson.Account;
              this.approvalCode = this.ccJson.AuthCode;
              if (this.approvalCode == '' || this.approvalCode == null) {
                this.approvalCode = (Math.floor(100000 + Math.random() * 900000)).toString();
              }
              break;
            }
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      } else {
        alert('Connection time out. Please try again or contact the administrator!');
      }
    });
  }

  async torDoScan() {
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false,
      prompt: 'Place a barcode inside the scan area',
      resultDisplayDuration: 500,
      orientation: 'portrait',
    };
    this.barcodeScanner.scan(options).then(async barcodeData => {
      this.scannedData = barcodeData;
      // tslint:disable-next-line: no-string-literal
      if (this.scannedData['text'].length == 4) {
        this.torData = this.scannedData['text'];
        this.torDate = new Date().toISOString();
      } else {
        alert(`Cashier Operator Number must be four digits.`)
      }
    }).catch(err => {
      console.log('Error', err);
    });
  }

  async getStorage(key): Promise<any> {
    try {
      const result = await this.storage.get(key);
      return result;
    }
    catch (e) { console.log(e); }
  }

  url(str) {
    const cleanUrl = JSON.parse(str);
    if (cleanUrl) {
      return environment.api.photo_url + cleanUrl[0].toString();
    }
  }

  startTime() {
    var intervalVar = setInterval(function () {
      this.today = new Date().toISOString();
    }.bind(this), 500);
  }

  get orderStatusList() {
    return this.orderStatusForm.get('orderStatus');
  }

  get notes() {
    return this.orderStatusForm.get('notes');
  }

  toggleDetails(data) {
    if (data.showDetails) {
      data.showDetails = false;
      data.icon = 'arrow-forward-circle-outline';
    } else {
      if (data.id == 1) {
        data.details = this.membershipNumber;
        console.log(data.details);
      } else if (data.id == 2) {
        data.details = `4`;
        console.log(data.details);
      } else if (data.id == 3) {
        const dividend = 10;
        console.log(this.product.length);
        if (((this.product.length / dividend) % 1) != 0) {
          console.log(`not`)
          this.maxSize = (Math.floor(this.product.length / dividend)) + 1;
        } else {
          console.log(`yes`)
          this.maxSize = (Math.floor(this.product.length / dividend));
        }
        this.qrOrderData = [];
        this.actualSize = dividend;
        this.curSize = 0;
        const d = this.product.slice(this.curSize, this.actualSize)
        /** 
        this.value = '';
        for (let i = 0; d.length-1>=i; i++){
          this.value = this.value + d[i];
        }
        */
        //console.log(`curSize: ${this.curSize}`);
        //console.log(`actualSize: ${this.actualSize}`);
        for (let i = 1; this.maxSize >= i; i++) {
          this.qrOrderData.push({
            id: i,
            curSize: this.curSize,
            actualSize: this.actualSize,
            isChecked: false,
          })
          this.curSize = this.actualSize;
          this.actualSize = this.actualSize + dividend;
        }
        console.log(this.qrOrderData);
      } else if (data.id == 4) {
        const value = this.jobOrderNo
        data.details = value;
        console.log(data.details);
      } else if (data.id == 5) {

      } else if (data.id == 6) {
        if (this.letScan) {
          data.details = `${this.cardNumber.slice(-4)}\n${this.expiryDate}\n${this.acquirerCode}\n${this.account}\n${this.approvalCode}`;
          console.log(data.details);
          data.details = data.details.trim();
          console.log(data.details);
          this.showQrDetails = true;
        }
      }
      data.showDetails = true;
      data.icon = 'arrow-down-circle-outline';
    }
  }

  nextDetails() {
    if (this.firstSize < 6) {
      this.value = ''
      this.firstSize = this.firstSize + 1;
      this.curSize = this.actualSize + 1;
      this.actualSize = this.actualSize + 20;
      const d = this.product.slice(this.curSize, this.actualSize);
      console.log(d)
      console.log(this.curSize);
      console.log(this.actualSize);
      if (d.length != 0) {
        for (let i = 0; d.length - 1 >= i; i++) {
          this.value = this.value + d[i];
        }
        console.log(`not 0`)
      } else {
        this.value = '';
        console.log(` 0`)
      }

      this.value = this.value.trim();
      console.log(this.value);
    }
  }

  prevDetails() {
    if (this.firstSize > 1) {
      this.value = ''
      this.firstSize = this.firstSize - 1;
      this.curSize = this.curSize - 20;
      this.actualSize = this.actualSize - 20;
      const d = this.product.slice(this.curSize, this.actualSize);
      console.log(this.curSize);
      console.log(this.actualSize);
      if (d.length != 0) {
        for (let i = 0; d.length - 1 >= i; i++) {
          this.value = this.value + d[i];
        }
      } else {
        this.value = '';
      }
      this.value = this.value.trim();
      console.log(this.value);
    }
  }


  adjust(bool) {
    if (this.letScan == false) {
      if (bool) {
        this.adjustment = true;
      } else {
        this.adjustment = false;
      }
    }
  }

  async onNewTotal(employeeID, password, newTotal) {
    let total;
    if (employeeID.value == '' || password.value == '' || newTotal.value == '') {
      alert(`Please input the missing fields.`);
    } else {
      const t = this.preauthorization_history.total.split(',').join("");
      if (Number(newTotal.value) <= Number(t)) {
        total = Number(newTotal.value);
        const params = {
          order_id: this.orderId,
          employee_id: employeeID.value,
          password: password.value,
          pos_total: total.toFixed(2)
        };

        await this.orderService.capturerPriceAdjustment(this.token, params).subscribe(async res => {
          // tslint:disable-next-line: no-string-literal
          console.log(res)
          if (res['success']) {
            let data = res['data']['cost_summary']['account_history'][res['data']['cost_summary']['account_history'].length - 1];
            let total = res['data']['cost_summary']['total'].split(',').join("");
            this.previousTotal = data['total'].split(',').join("");
            this.posTotal = Number(newTotal.value).toFixed(2);
            this.adjustmentTotal = Number(this.posTotal - total).toFixed(2);
            this.adjustment = false;
            this.isPayment = true;
            this.paymentColor = 'primary';
            this.getCostSummary();
          } else {
            alert(res['message']);
          }
        });
      } else {
        alert(`The new total must be less than or equal to the preauth total.`);
      }

    }
  }

  async forPayment() {
    if (this.letScan == false) {
      const params = {
        job_order_number: this.jobOrderNo,
        captured_amount: this.posTotal
      };
      console.log(params)
      this.orderService.capturerPayment(this.token, params).subscribe(async (res) => {
        // tslint:disable-next-line: no-string-literal
        console.log(res);
        if (res['success']['captured']) {
          console.log(res);
          const data = JSON.parse(res['data']['capture_history']);
          if (data['TxnStatus'] == '0') {
            this.getCostSummary();
            this.letScan = true;
            this.paymentColor = 'medium';
            this.adjustColor = 'medium';
          } else if (data['TxnStatus'] == '1') {
            this.getCostSummary();
            this.letScan = true;
            this.paymentColor = 'medium';
            this.adjustColor = 'medium';
          } else {
            //this.letScan = true;
            alert(data['TxnMessage']);
          }
        } else {
          alert(res['message']);
        }
        //this.letScan = false;
      });
    }
  }

  async openInputModal(status) {
    const modal = await this.modalController.create({
      component: QuantityModalPage,
      componentProps: {}
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned != null) {
        if (status == 1) {
          if (dataReturned.data.length == 8) {
            this.encodedData = dataReturned.data;
            this.today = new Date().toISOString();
          } else {
            const d = dataReturned.data;
            alert(`Invalid receipt number ${d}`);
          }
        } else if (status == 2) {
          if (dataReturned.data.length == 4) {
            this.torData = dataReturned.data;
            this.torDate = new Date().toISOString();
          } else {
            alert(`Cashier Operator Number must be four digits.`)
          }
        }
      }
    });
    return await modal.present();
  }

  counter(i: number) {
    return new Array(i);
  }

}
