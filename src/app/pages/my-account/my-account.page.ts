import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { MyAccountService } from '../../services/my-account.service';
import { FormGroup, FormControl } from '@angular/forms';
import { MyAccount } from '../../models/my-account';
import { AutoLogoutService } from '../../services/auto-logout.service';
@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.page.html',
  styleUrls: ['./my-account.page.scss'],
})
export class MyAccountPage implements OnInit {
  token: any
  myAccountForm: FormGroup;
  myAccount: MyAccount[] = [];
  username: any;
  firstname: any;
  middlename: any;
  lastname: any;
  emailaddress: any;
  mobileno: any;

  constructor(private storage: Storage,
              private myAccountService: MyAccountService,
              private autoLogout: AutoLogoutService) {}

  async ngOnInit() {
    await this.storage.get('token').then((val) => {this.token = val;});
    await this.myAccountService.getDetails(this.token).subscribe(res => {
      if(res['success']){
        const data = res['success'];
        this.username = data['employee_id'];
        this.firstname = data['firstname'];
        this.middlename = data['middlename'];
        this.lastname = data['lastname'];
        this.emailaddress = data['email'];
        this.mobileno = data['mobile_no'];
      }else{
        alert('Connection time out. Please contact the administrator!');
      }  
    });
  }
}
