import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { AutoLogoutService } from '../../../services/auto-logout.service';@Component({
  selector: 'app-search-modal',
  templateUrl: './search-modal.page.html',
  styleUrls: ['./search-modal.page.scss'],
})
export class SearchModalPage implements OnInit {
  constructor(private modalController: ModalController,
    private autoLogout: AutoLogoutService) { }

  ngOnInit() {
  }

  async closeModal() {
    await this.modalController.dismiss(null);
  }

  async doSearchViaUPC(upcNew){
    if(upcNew){
      await this.modalController.dismiss(upcNew);
    }else{
      alert('Please input the UPC#');
    }
  }
}
