import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrderService } from '../../../services/order.service';
import { DataServiceService } from '../../../services/data-service.service';
import { Storage } from '@ionic/storage';
import { Product } from '../../../models/product';
import { TempProduct } from '../../../models/temp-product';
import { ProductData } from '../../../models/product-data';
import { OrderStatus } from '../../../models/order-status';
import { ActivatedRoute, Router } from '@angular/router';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { ModalController } from '@ionic/angular';
import { QuantityModalPage } from '../quantity-modal/quantity-modal.page';
import { ReplaceModalPage } from '../replace-modal/replace-modal.page';
import { SearchModalPage } from '../search-modal/search-modal.page';
import { ImageModalPage } from '../image-modal/image-modal.page';
import { AutoLogoutService } from '../../../services/auto-logout.service';
import { AlertController } from '@ionic/angular';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})

export class ProductsPage implements OnInit {
  status: any;
  segmentModel = 'order';
  token: any;
  // tslint:disable-next-line: variable-name
  shoppers_id: any;
  products: Product[] = [];
  tempProducts: TempProduct[] = [];
  productDatas: ProductData[] = [];
  orderStatus: OrderStatus[] = [];
  today: any;
  orderId: any;
  scannedData: any;
  encodedData: '';
  encodeData: any;
  dataQuantityReturned: any;
  dataProductReturned: Product[] = [];
  replaceData: ProductData[] = [];
  jobOrderNo: any;
  areaOfDelivery: any = '';
  deliveryDate: any;
  orderStatusForm: FormGroup;
  OrderStatusId: any;
  total = 0;
  CustomerName: any;
  orders: any[] = [];
  deliveryLevel: any;
  deliveryDate1: any;
  deliveryDate2: any;
  cost_summary: any;
  membershipNumber = '009999999909';
  membershipStatus: any;
  hasMembership: any;
  title: any;
  gender: string;
  email: any;
  mobileNo = 'N/A';
  landlineNo = 'N/A';
  createdAt: any;
  birthdate: string;
  preauth: any;
  preauthorization_history: any;
  constructor(private barcodeScanner: BarcodeScanner,
    private orderService: OrderService,
    private storage: Storage,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dataServiceService: DataServiceService,
    public modalController: ModalController,
    private autoLogout: AutoLogoutService,
    public alertController: AlertController) {
    this.status = 'order-details';
    this.orderId = this.activatedRoute.snapshot.paramMap.get('id');
    this.orderStatusForm = this.formBuilder.group({
      orderStatus: ['', [Validators.required]],
      notes: [''],
    });
  }
  async ngOnInit() {
    this.token = await this.getStorage('token');
    this.shoppers_id = await this.getStorage('shoppersId');
    this.orders = await this.getStorage('order');
    for (const order of this.orders) {
      this.jobOrderNo = order.job_order_number;
      this.hasMembership = order.has_membership;
      this.cost_summary = order.cost_summary;
      this.preauthorization_history = order.preauthorization_history;
      this.preauth = this.preauthorization_history.total;
      if (order.shipping_company_name) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_company_name}`;
      }
      if (order.shipping_building_no) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_building_no}`;
      }
      if (order.shipping_street_name) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_street_name}`;
      }
      if (order.shipping_barangay) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_barangay}`;
      }
      if (order.shipping_city) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_city}`;
      }
      if (this.hasMembership) {
        this.membershipStatus = order.membership_status.membership_status;
        if (order.membership_status.membership_number != 'N/A') {
          this.membershipNumber = order.membership_status.membership_number;
        }
      }
      if (order.member) {
        this.title = order.member.title;
        if (order.member.title == 'Mr.') {
          this.gender = 'Male'
        } else if (order.member.title == 'Ms.') {
          this.gender = 'Female'
        }
        this.email = order.member.email;
        if (order.member.mobile_number != null) {
          this.mobileNo = order.member.mobile_number;
        }
        if (order.member.landline_number != null) {
          this.landlineNo = order.member.landline_number;
        }
        this.createdAt = order.member.created_at;
        this.birthdate = `${order.member.birthdate_month}/${order.member.birthdate_day}/${order.member.birthdate_year}`;
        this.createdAt = order.member.created_at;
      }
      this.deliveryDate1 = order.delivery_date_time;
      this.deliveryDate2 = order.delivery_date_time2;
      this.deliveryLevel = order.delivery_level;
      this.deliveryDate = order.delivery_date_time;
      this.CustomerName = `${order.shipping_first_name} ${order.shipping_last_name}`;
      this.orderStatusForm.get('orderStatus').setValue(`${order.order_status_id}`);
    }
    if (this.preauth == null || this.preauth == '') {
      await this.dataServiceService.setpreauth(this.cost_summary.total);
    }
    await this.getData();
  }

  async doUpdate() {
    // tslint:disable-next-line: triple-equals
    if (this.orderStatusList.value == 4) {
      let count = 0;
      for (const data of this.productDatas) {
        if (data.color == 'success') {
          count = count + 1;
        }
      }
      if (count == this.productDatas.length && this.cost_summary.subtotal != 0) {
        /** 
        const capturedAmount = Number(parseFloat(this.cost_summary.total.replace(/,/g, '')).toFixed(2));
        const capture = {
          order_id: Number(this.orderId),
          captured_amount : capturedAmount
        }
        await this.orderService.capturerOrders(this.token, capture).subscribe(async res => {
          if(res['captured']){
            const params = {
              order_id: this.orderId,
              order_status_id: this.orderStatusList.value,
              remarks: this.notes.value
            };
            await this.orderService.updateOrder(this.token, params).subscribe(async res => {
              // tslint:disable-next-line: no-string-literal
              if (res['success']){
                // tslint:disable-next-line: no-string-literal
                alert(res['message']);
                await this.dataServiceService.setOrderStatusId(this.orderStatusList.value);
                await this.router.navigate(['menu/pos-order']);
                //await this.router.navigate(['menu/pos-order/products', this.orderId]);
                console.log('Order update successfully');
              }
            });
          }else{
            alert(res['message']);
          }
        });
        */
        /**
         */
        const params = {
          order_id: this.orderId,
          order_status_id: this.orderStatusList.value,
          remarks: this.notes.value
        };
        await this.orderService.updateOrder(this.token, params).subscribe(async res => {
          if (res['success']) {
            await this.dataServiceService.setOrderStatusId(this.orderStatusList.value);
            await this.router.navigate(['menu/pos-order']);
            console.log('Order update successfully');
          }
        });
      } else {
        alert(`You cannot proceed to POS, please check your items.`);
      }
      /** 
      }else if (this.orderStatusList.value == 8){
        alert(`Order has been placed on hold.`);
        const params = {
              order_id: this.orderId,
              order_status_id: this.orderStatusList.value,
              remarks: this.notes.value
            };
        await this.orderService.updateOrder(this.token, params).subscribe(async res => {
          if (res['success']){
            await this.router.navigate(['menu/hold-order']);
            console.log('Order update successfully');
          }
        });
      }else if (this.orderStatusList.value == 9){
          const params = {
                order_id: this.orderId,
                order_status_id: this.orderStatusList.value,
                remarks: this.notes.value
              };
          await this.orderService.updateOrder(this.token, params).subscribe(async res => {
            if (res['success']){
              await this.router.navigate(['menu/cancel-order']);
              console.log('Order update successfully');
            }
          });
          alert(`Order has been cancelled.`);
      }*/
    }
  }

  async getOrderStatusList() {
    await this.orderService.getOrderStatusList(this.token).subscribe(async res => {
      if (res) {
        // tslint:disable-next-line: no-string-literal
        if (res['success']) {
          // tslint:disable-next-line: no-string-literal
          this.orderStatus.push({ success: res['success'], data: res['data'], message: res['message'], code: res['code'] });
        }
      }
    });
  }

  async updateProductStock(index) {
    let params = {
      order_id: this.orderId,
      product_cart_id: this.productDatas[index].id,
      is_out_of_stock: 1
    }
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert!',
      message: 'Do you want to keep going?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'danger',
          handler: (blah) => {

          }
        }, {
          text: 'Yes',
          handler: async () => {
            if (this.productDatas[index].product_type_id == '2') {
              const params = {
                order_id: this.orderId,
                cart_products_id: this.productDatas[index].id + '',
                upc: `270${this.productDatas[index].sku}000000`
              };
              console.log(params);
              this.orderService.updateWeighted(this.token, params).subscribe(async res => {
                console.log(res);
                // tslint:disable-next-line: no-string-literal
                if (res['success']) {
                  const result = `270${this.productDatas[index]}000000`;
                  const tempWeight = result.substr(result.length - 6);
                  const weight = tempWeight.substring(0, 5);
                  const actualWeight = Number(weight) / 1000;
                  this.productDatas[index].upc = `270${this.productDatas[index]}000000`
                  this.productDatas[index].status = 3;
                  this.productDatas[index].color = 'success';
                  this.productDatas[index].is_out_of_stock = 1;
                  this.productDatas[index].sub_product_id = res['data']['sub_product']['id'];
                  this.productDatas[index].weight = actualWeight.toString();
                  this.total = this.total - 1;
                  this.getCostSummary();
                }
              });
            } else {
              await this.orderService.updateProductStock(this.token, params).subscribe(async res => {
                if (res['success']) {
                  this.productDatas[index].status = 0;
                  this.productDatas[index].is_out_of_stock = 1;
                  this.productDatas[index].color = 'success';
                  this.total = this.total - Number(this.productDatas[index].quantity);
                  this.getCostSummary();
                }
              });
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async resetProductStock(index) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert!',
      message: 'Do you want to keep going?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'danger',
          handler: (blah) => {

          }
        }, {
          text: 'Yes',
          handler: async () => {
            if (this.productDatas[index].product_type_id == '2') {
              let params = {
                order_id: this.orderId,
                sub_product_id: this.productDatas[index].sub_product_id
              }
              await this.orderService.deleteWeightedProductStock(this.token, params).subscribe(async res => {
                if (res['success']) {
                  this.productDatas[index].status = 0;
                  this.productDatas[index].weight = '1';
                  this.productDatas[index].is_out_of_stock = 0;
                  this.productDatas[index].color = 'primary';
                  this.total = this.total + Number(this.productDatas[index].quantity);
                  this.getCostSummary();
                } else {
                  console.log(res);
                }
              });
            } else {
              let params = {
                order_id: this.orderId,
                product_cart_id: this.productDatas[index].id,
                is_out_of_stock: 0
              }
              await this.orderService.updateProductStock(this.token, params).subscribe(async res => {
                if (res['success']) {
                  this.productDatas[index].status = 0;
                  this.productDatas[index].is_out_of_stock = 0;
                  this.productDatas[index].color = 'primary';
                  this.total = this.total + Number(this.productDatas[index].quantity);
                  this.getCostSummary();
                }
              });
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async openQuantityModal(index) {
    const modal = await this.modalController.create({
      component: QuantityModalPage,
      componentProps: {
        quantity: this.productDatas[index].quantity,
        sku: this.productDatas[index].sku,
        upc: this.productDatas[index].upc,
        orderId: this.orderId,
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned != null) {
        this.dataQuantityReturned = dataReturned.data;
        if (this.dataQuantityReturned != null) {
          if (this.productDatas[index].quantity != this.dataQuantityReturned) {
            this.productDatas[index].isEdited = 1;
            this.productDatas[index].quantity = this.dataQuantityReturned;
            this.total = 0;
            for (const data of this.productDatas) {
              if (data.isReplaced == 1) { }
              else if (data.is_out_of_stock == 1) { }
              else {
                this.total = this.total + Number(data.quantity);
                this.getCostSummary();
              }
            }
            console.log(this.total);
          }
        }
      }
    });
    this.getCostSummary();
    return await modal.present();
  }

  goToList() {
    this.router.navigate(['/menu/process-order']);
  }

  async openReplaceModal(index) {
    this.dataProductReturned = [];
    this.replaceData = [];
    let upc = '';
    let skus: any[] = [];
    const qty = this.productDatas[index].quantity;
    if (this.products[0].data[index].inventory.products.upc.length > 0) {
      upc = this.products[0].data[index].inventory.products.upc[0].upc;
    }
    const modal = await this.modalController.create({
      component: ReplaceModalPage,
      componentProps: {
        sku: this.productDatas[index].sku,
        // tslint:disable-next-line: object-literal-shorthand
        upc: upc,
        productId: this.productDatas[index].id,
        orderId: this.orderId,
      }
    });
    modal.onDidDismiss().then(async (dataReturned) => {
      if (dataReturned != null) {
        this.dataProductReturned = dataReturned.data;
        if (this.dataProductReturned != null) {
          for (const x of dataReturned.data) {
            if (x.data) {
              // tslint:disable-next-line: no-shadowed-variable
              let upc = '';
              this.productDatas[index].status = 0;
              this.productDatas[index].quantity = '0';
              this.productDatas[index].isReplaced = 1;
              this.productDatas[index].color = 'success';
              if (x.data.inventory.products.upc && x.data.inventory.products.upc.length > 0) {
                upc = x.data.inventory.products.upc[0].upc;
              }

              this.replaceData.push({
                quantity: qty,
                is_same_prod_diff_size: x.data.is_same_prod_diff_size,
                is_same_size_diff_brand: x.data.is_same_size_diff_brand,
                is_no_substitution: x.data.is_no_substitution,
                is_out_of_stock: x.data.is_out_of_stock,
                is_dc_delivery: x.data.is_dc_delivery,
                delivery_date_time: x.data.dc_schedule,
                id: x.data.id,
                product_id: x.data.product_id,
                sku: x.data.inventory.sku,
                department_id: x.data.inventory.department_id,
                sku_location: x.data.inventory.products.sku_location,
                name: x.data.inventory.products.name,
                featured_image: this.url(x.data.inventory.products.featured_image),
                product_type_id: x.data.inventory.products.product_type_id,
                sub_product_id: 0,
                upc: upc,
                price_history: x.data.price_history,
                weight: '1',
                color: 'success',
                isEdited: 0,
                isReplaced: 0,
                status: 1,
              });
              skus.push(x.data.inventory.sku);
              this.productDatas.splice(index + 1, 0, this.replaceData[0]);
              for (const products of this.products) {
                products.data.splice(index + 1, 0, x.data);
              }
              this.total = 0;
              for (const data of this.productDatas) {
                if (data.isReplaced == 1) { }
                else if (data.is_out_of_stock == 1) {
                } else {
                  this.total = this.total + Number(data.quantity);
                }
              }
            }
          }
          await this.orderService.getAccessToken().subscribe(async res => {
            await this.orderService.getSKULocation(res['access_token'], skus).subscribe(async res => {
              for (const data of res) {
                for (const proData of this.productDatas) {
                  if (proData.sku == data.sku.toString()) {
                    proData.sku_location = data.location;
                  }
                }
              }
            });
          });

          this.getCostSummary();
        }
      }
    });
    return await modal.present();
  }

  async getData(): Promise<any> {
    let result;
    let skus: any[] = [];
    try {
      const params = {
        order_id: this.orderId
      };
      await this.orderService.getOrderProducts(this.token, params).subscribe(async res => {
        if (res) {
          if (res['success']) {
            this.products.push({ success: res['success'], data: res['data'], message: res['message'], code: res['code'] });
            for (const product of this.products) {
              for (const data of product.data) {
                if ((data.deleted_at == null && data.is_replaced == 0) || (data.deleted_at != null && data.is_replaced == 1)) {
                  let isReplaced = 0;
                  let isOutOfStock = 0;
                  let status = 0;
                  let color = 'primary';
                  if (data.is_replaced == 1) {
                    isReplaced = 1;
                    color = 'success';
                    status = 2;
                  } else if (data.is_out_of_stock == 1) {
                    isOutOfStock = 1;
                    color = 'success';
                    status = 3;
                  } else {
                    if (data.inventory.sku == "136") {
                      color = 'success';
                      status = 2;
                    }
                    this.total = this.total + Number(data.quantity);
                  }
                  //start of fresh items
                  if (data.inventory.products.product_type_id == "2") {
                    if (data.is_replaced == 1) {
                      this.productDatas.push({
                        quantity: data.quantity,
                        is_same_prod_diff_size: data.is_same_prod_diff_size,
                        is_same_size_diff_brand: data.is_same_size_diff_brand,
                        is_no_substitution: data.is_no_substitution,
                        is_out_of_stock: isOutOfStock,
                        is_dc_delivery: data.is_dc_delivery,
                        delivery_date_time: data.dc_schedule,
                        id: data.id,
                        product_id: data.product_id,
                        sku: data.inventory.sku,
                        department_id: data.inventory.department_id,
                        sku_location: data.inventory.products.sku_location,
                        name: data.inventory.products.name,
                        featured_image: this.url(data.inventory.products.featured_image),
                        product_type_id: data.inventory.products.product_type_id,
                        sub_product_id: 0,
                        upc: data.scanned_upc,
                        price_history: data.price_history,
                        weight: '1',
                        color: color,
                        isEdited: 0,
                        isReplaced: isReplaced,
                        status: status
                      });
                      skus.push(data.inventory.sku);
                    } else {
                      for (let i = 1; i <= Number(data.quantity); i++) {
                        if (data.sub_products.length > 0) {
                          let weight;
                          let sub_product_id;
                          if (data.sub_products.length >= i) {
                            if (data.sub_products[i - 1].weight != '0') {
                              weight = data.sub_products[i - 1].weight;
                              sub_product_id = data.sub_products[i - 1].id;
                              color = 'success';
                              status = 1;
                              isOutOfStock = 0;
                            } else {
                              weight = data.sub_products[i - 1].weight;
                              sub_product_id = data.sub_products[i - 1].id;
                              isOutOfStock = 1;
                              color = 'success';
                              status = 3;
                              this.total = this.total - 1;
                            }
                          } else {
                            weight = '1';
                            sub_product_id = 0;
                            isOutOfStock = 0;
                            color = 'primary';
                            status = 0;
                          }
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            sub_product_id: sub_product_id,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: weight,
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                          skus.push(data.inventory.sku);
                        } else {
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            sub_product_id: 0,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: '1',
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                          skus.push(data.inventory.sku);
                        }
                      }
                    }
                  } else {
                    if (data.scanned_upc != null) {
                      color = 'success';
                      status = 1;
                    }
                    this.productDatas.push({
                      quantity: data.quantity,
                      is_same_prod_diff_size: data.is_same_prod_diff_size,
                      is_same_size_diff_brand: data.is_same_size_diff_brand,
                      is_no_substitution: data.is_no_substitution,
                      is_out_of_stock: isOutOfStock,
                      is_dc_delivery: data.is_dc_delivery,
                      delivery_date_time: data.dc_schedule,
                      id: data.id,
                      product_id: data.product_id,
                      sku: data.inventory.sku,
                      department_id: data.inventory.department_id,
                      sku_location: data.inventory.products.sku_location,
                      name: data.inventory.products.name,
                      featured_image: this.url(data.inventory.products.featured_image),
                      product_type_id: data.inventory.products.product_type_id,
                      sub_product_id: 0,
                      upc: data.scanned_upc,
                      price_history: data.price_history,
                      weight: '1',
                      color: color,
                      isEdited: 0,
                      isReplaced: isReplaced,
                      status: status
                    });
                    skus.push(data.inventory.sku);
                  }
                  //end of fresh items
                }

              }
              await this.getOrderStatusList();
            }
            await this.orderService.getAccessToken().subscribe(async res => {
              await this.orderService.getSKULocation(res['access_token'], skus).subscribe(async res => {
                for (const data of res) {
                  for (const proData of this.productDatas) {
                    if (proData.sku == data.sku.toString()) {
                      proData.sku_location = data.location;
                    }
                  }
                }
              });
            });
          } else {
            alert('Connection time out. Please try again or contact the administrator!');
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
      return result;
    }
    catch (e) { console.log(e); }
  }

  async doScanReplace(id, index) {
    this.tempProducts = [];
    this.replaceData = [];
    let skus: any[] = [];
    const qty = this.productDatas[index].quantity;
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false,
      prompt: 'Place a barcode inside the scan area',
      resultDisplayDuration: 500,
      orientation: 'portrait',
    };
    this.barcodeScanner.scan(options).then(async barcodeData => {
      this.scannedData = barcodeData;
      if (this.scannedData['text']) {
        const res = this.scannedData['text'];
        const upc_new = res.replace(/^0+/, '');
        const params = {
          order_id: this.orderId,
          product_id: id,
          upc_new: upc_new
        };
        this.token = await this.getStorage('token');
        await this.orderService.replaceOrder(this.token, params).subscribe(async (res) => {
          if (res['success']) {
            if (Object.keys(res['data']).length > 0) {
              this.tempProducts.push({ success: res['success'], data: res['data'], message: res['message'], code: res['code'] });
              for (const temp of this.tempProducts) {
                this.productDatas[index].status = 0;
                this.productDatas[index].quantity = '0';
                this.productDatas[index].isReplaced = 1;
                this.productDatas[index].color = 'success';
                let deliveryDate;
                let schedule;
                if (temp.data.schedule1 == 1) {
                  deliveryDate = this.deliveryDate1;
                  schedule = 1;
                } else if (temp.data.schedule1 == 2) {
                  deliveryDate = this.deliveryDate2;
                  schedule = 2;
                }
                this.replaceData.push({
                  quantity: qty,
                  is_same_prod_diff_size: temp.data.is_same_prod_diff_size,
                  is_same_size_diff_brand: temp.data.is_same_size_diff_brand,
                  is_no_substitution: temp.data.is_no_substitution,
                  is_out_of_stock: temp.data.is_out_of_stock,
                  is_dc_delivery: temp.data.is_dc_delivery,
                  delivery_date_time: temp.data.dc_schedule,
                  id: temp.data.id,
                  product_id: temp.data.product_id,
                  sku: temp.data.inventory.sku,
                  department_id: temp.data.inventory.department_id,
                  sku_location: temp.data.inventory.products.sku_location,
                  name: temp.data.inventory.products.name,
                  featured_image: this.url(temp.data.inventory.products.featured_image),
                  product_type_id: temp.data.inventory.products.product_type_id,
                  sub_product_id: 0,
                  upc: upc_new,
                  price_history: temp.data.price_history,
                  weight: '1',
                  color: 'success',
                  isEdited: 0,
                  isReplaced: 0,
                  status: 2,
                });
                skus.push(temp.data.inventory.sku);
                this.productDatas.splice(index + 1, 0, this.replaceData[0]);
                for (const products of this.products) {
                  products.data.splice(index + 1, 0, temp.data);
                }
                this.total = 0;
              }
              await this.orderService.getAccessToken().subscribe(async res => {
                await this.orderService.getSKULocation(res['access_token'], skus).subscribe(async res => {
                  this.total = 0;
                  for (const data of res) {
                    for (const proData of this.productDatas) {
                      if (proData.isReplaced == 1) { }
                      else if (proData.is_out_of_stock == 1) {
                      } else {
                        this.total = this.total + Number(proData.quantity);
                      }
                    }
                  }
                });
              });
              this.getCostSummary();
              alert(res['message']);
            } else {
              alert(res['message']);
            }
          } else {
            alert(res['message']);
          }
        });
      }
    }).catch(err => {
      console.log('Error', err);
    });
  }

  doScan(id, index) {
    if (this.productDatas[index].product_type_id == '2') {
      const options: BarcodeScannerOptions = {
        preferFrontCamera: false,
        showFlipCameraButton: true,
        showTorchButton: true,
        torchOn: false,
        prompt: 'Place a barcode inside the scan area',
        resultDisplayDuration: 500,
        orientation: 'portrait',
      };
      this.barcodeScanner.scan(options).then(async barcodeData => {
        this.scannedData = barcodeData;
        let flag = 0;
        // tslint:disable-next-line: no-string-literal
        const result = this.scannedData['text'];
        if (result != null) {
          if (result.length == 13) {
            if (result.search(`270${this.productDatas[index].sku}`) >= 0) {
              const upc = result.replace(/^0+/, '');
              const params = {
                order_id: this.orderId,
                cart_products_id: this.productDatas[index].id + '',
                upc: result
              };
              await this.orderService.updateWeighted(this.token, params).subscribe(async res => {
                // tslint:disable-next-line: no-string-literal
                if (res['success']) {
                  const tempWeight = result.substr(result.length - 6);
                  const weight = tempWeight.substring(0, 5);
                  const actualWeight = weight / 1000;
                  this.productDatas[index].weight = actualWeight.toString();
                  this.productDatas[index].upc = result
                  this.productDatas[index].status = 1;
                  this.productDatas[index].color = 'success';
                  this.getCostSummary();
                } else {
                  this.productDatas[index].status = 2;
                  this.productDatas[index].color = 'danger';
                  alert(res['message']);
                }
              });
            } else {
              alert(`You scanned a different barcode.`);
            }
          } else {
            alert(`Invalid upc#: ${result}`);
          }
        } else {
          alert('Please try scanning again the barcode result value is null.');
        }
      }).catch(err => {
        console.log('Error', err);
      });
    } else {
      const options: BarcodeScannerOptions = {
        preferFrontCamera: false,
        showFlipCameraButton: true,
        showTorchButton: true,
        torchOn: false,
        prompt: 'Place a barcode inside the scan area',
        resultDisplayDuration: 500,
        orientation: 'portrait',
      };
      this.barcodeScanner.scan(options).then(barcodeData => {
        this.scannedData = barcodeData;
        let flag = 0;
        const res = this.scannedData['text'];
        for (const product of this.products) {
          for (const data of product.data) {
            if (data.id == id) {
              if (Object.keys(data.inventory.products.upc).length > 0) {
                for (const upc of data.inventory.products.upc) {
                  if (upc.upc == res.replace(/^0+/, '')) {
                    flag = 1;
                    break;
                  }
                }
                if (flag == 0) {
                  flag = 2;
                }
              } else {
                flag = 3;
              }
              break;
            }
          }
        }
        if (flag == 1) {
          this.productDatas[index].upc = res.replace(/^0+/, '');
          const params = {
            order_id: this.orderId,
            upc: res.replace(/^0+/, ''),
            quantity: this.productDatas[index].quantity
          };
          const resUpc = res.replace(/^0+/, '');
          // tslint:disable-next-line: no-shadowed-variable
          this.orderService.updateOrderProduct(this.token, params).subscribe(async res => {
            // tslint:disable-next-line: no-string-literal
            if (res['success']) {
              this.productDatas[index].status = 1;
              this.productDatas[index].color = 'success';
              this.getCostSummary();
            } else {
              alert(res['message']);
            }
          });
        } else if (flag == 2) {
          this.productDatas[index].status = 2;
          this.productDatas[index].color = 'danger';
          alert(`Barcode Scan: ${res}. You scanned a different barcode.`);
        } else if (flag == 3) {
          this.productDatas[index].status = 2;
          this.productDatas[index].color = 'danger';
          alert(`SKU#: ${this.productDatas[index].sku} on the product list has no UPC data.`);
        }
      }).catch(err => {
        console.log('Error', err);
      });
    }
  }

  async doSearch(id, index) {
    if (this.productDatas[index].product_type_id == '2') {
      const modal = await this.modalController.create({
        component: SearchModalPage,
        componentProps: {}
      });
      modal.onDidDismiss().then(async (dataReturned) => {
        if (dataReturned != null) {
          const result = dataReturned.data;
          if (result != null) {
            if (result.search(`270${this.productDatas[index].sku}`) >= 0) {
              const upc = result.replace(/^0+/, '');
              const params = {
                order_id: this.orderId,
                cart_products_id: this.productDatas[index].id + '',
                upc: result
              };
              await this.orderService.updateWeighted(this.token, params).subscribe(async res => {
                if (res['success']) {
                  const tempWeight = result.substr(result.length - 6);
                  const weight = tempWeight.substring(0, 5);
                  const actualWeight = weight / 1000;
                  this.productDatas[index].weight = actualWeight.toString();
                  this.productDatas[index].upc = result
                  this.productDatas[index].status = 1;
                  this.productDatas[index].color = 'success';
                  this.getCostSummary();
                } else {
                  this.productDatas[index].status = 2;
                  this.productDatas[index].color = 'danger';
                  alert(res['message']);
                }
              });
            } else {
              alert(`You scanned a different barcode.`);
            }
          }
        }
      });
      return await modal.present();
    } else {
      const modal = await this.modalController.create({
        component: SearchModalPage,
        componentProps: {}
      });
      modal.onDidDismiss().then(async (dataReturned) => {
        if (dataReturned != null) {
          const result = dataReturned.data;
          if (result != null) {
            let flag = 0;
            for (const product of this.products) {
              for (const data of product.data) {
                if (data.id == id) {
                  if (data.inventory.products.upc) {
                    for (const upc of data.inventory.products.upc) {
                      if (upc.upc == result.replace(/^0+/, '')) {
                        flag = 1;
                        break;
                      }
                    }
                    if (flag == 0) {
                      flag = 2;
                    }
                  } else {
                    flag = 3;
                  }
                  break;
                }
              }
            }
            if (flag == 1) {
              this.productDatas[index].upc = result.replace(/^0+/, '');
              const params = {
                order_id: this.orderId,
                upc: result.replace(/^0+/, ''),
                quantity: this.productDatas[index].quantity
              };
              await this.orderService.updateOrderProduct(this.token, params).subscribe(async res => {
                if (res['success']) {
                  this.productDatas[index].status = 1;
                  this.productDatas[index].color = 'success';
                  console.log('Order update successfully');
                  this.getCostSummary();
                } else {
                  alert(res['message']);
                }
              });
            } else if (flag == 2) {
              this.productDatas[index].status = 2;
              this.productDatas[index].color = 'danger';
              alert(`You scanned a different barcode.`);
            } else if (flag == 3) {
              this.productDatas[index].status = 2;
              this.productDatas[index].color = 'danger';
              alert(`SKU#: ${this.productDatas[index].sku} on the product list has no UPC data.`);
            }
          }
        }
      });
      return await modal.present();
    }
  }

  async openImage(img) {
    const modal = await this.modalController.create({
      component: ImageModalPage,
      componentProps: {
        img
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
    });
    return await modal.present();
  }

  url(str) {
    const cleanUrl = JSON.parse(str);
    if (cleanUrl) {
      return environment.api.photo_url + cleanUrl[0].toString();
    }
  }

  async getCostSummary() {
    const params = {
      shoppers_id: this.shoppers_id
    };
    await this.orderService.getAvailableOnProcessOrders(this.token, params).subscribe(async res => {
      if (res) {
        if (res['success']) {
          for (const data of res['data']) {
            if (this.orderId == data['id']) {
              const output = JSON.parse(data['cost_summary']);
              this.cost_summary = {
                subtotal: output['subtotal'],
                concierge: output['concierge'],
                shipping_fee: output['shipping_fee'],
                non_member_surcharge: output['non_member_surcharge'],
                total: output['total']
              }
              break;
            }
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      } else {
        alert('Connection time out. Please try again or contact the administrator!');
      }
    });
  }


  async getStorage(key): Promise<any> {
    try {
      const result = await this.storage.get(key);
      return result;
    }
    catch (e) { console.log(e); }
  }

  startTime() {
    const intervalVar = setInterval(function () {
      this.today = new Date().toISOString();
    }.bind(this), 500);
  }

  get f() { return this.orderStatusForm.controls; }
  get orderStatusList() {
    return this.orderStatusForm.get('orderStatus');
  }

  get notes() {
    return this.orderStatusForm.get('notes');
  }

}
