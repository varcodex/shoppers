import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReplaceModalPage } from './replace-modal.page';

describe('ReplaceModalPage', () => {
  let component: ReplaceModalPage;
  let fixture: ComponentFixture<ReplaceModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplaceModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReplaceModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
