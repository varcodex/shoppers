import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReplaceModalPage } from './replace-modal.page';

const routes: Routes = [
  {
    path: '',
    component: ReplaceModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReplaceModalPageRoutingModule {}
