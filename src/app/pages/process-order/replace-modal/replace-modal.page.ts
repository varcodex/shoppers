import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { OrderService } from '../../../services/order.service';
import { Storage } from '@ionic/storage';
import { Product } from '../../../models/product';
import { AutoLogoutService } from '../../../services/auto-logout.service';
@Component({
  selector: 'app-replace-modal',
  templateUrl: './replace-modal.page.html',
  styleUrls: ['./replace-modal.page.scss'],
})
export class ReplaceModalPage implements OnInit {
  token: any;
  orderId: any;
  segmentModel = 'upc';
  products: Product[] = [];
  sku: any;
  upc: any;
  productId: any;
  constructor(private modalController: ModalController,
              private navParams: NavParams,
              private orderService: OrderService,
              private storage: Storage,
              private autoLogout: AutoLogoutService) { }

  async ngOnInit() {
    this.token = await this.getStorage('token');
    this.orderId = this.navParams.data.orderId;
    this.sku = this.navParams.data.sku;
    this.upc = this.navParams.data.upc;
    this.productId = this.navParams.data.productId;
  }

  async closeModal() {
    await this.modalController.dismiss(null);
  }

  async doReplaceViaSKU(skuNew){
    if (skuNew){
      const params = {
        order_id: this.orderId,
        product_id: this.productId,
        sku_new: skuNew
      };
      this.token = await this.getStorage('token');
      await this.orderService.replaceOrder(this.token, params).subscribe(async res => {
        // tslint:disable-next-line: no-string-literal
        if (res['success']){
          // tslint:disable-next-line: no-string-literal
          if (Object.keys(res['data']).length > 0){
              console.log('data is not empty');
              // tslint:disable-next-line: no-string-literal
              alert(res['message']);
              // tslint:disable-next-line: no-string-literal
              this.products.push({success : res['success'], data : res['data'], message : res['message'], code: res['code']});
              await this.modalController.dismiss(this.products);
          }else{
            this.products = [];
            console.log('data is empty');
            // tslint:disable-next-line: no-string-literal
            alert(res['message']);
          }
        }else{
          alert(res['message']);
        }
      });
    }else{
      alert('Please input the SKU#');
    }
  }

  async doReplaceViaUPC(upcNew){
    if (upcNew){
      const upc = upcNew.replace(/^0+/, '');
      const params = {
          order_id: this.orderId,
          product_id: this.productId,
          upc_new: upc
        };
      this.token = await this.getStorage('token');
      await this.orderService.replaceOrder(this.token, params).subscribe(async res => {
          console.log();
          // tslint:disable-next-line: no-string-literal
          if (res['success']){
            // tslint:disable-next-line: no-string-literal
            if (Object.keys(res['data']).length > 0){
              console.log('data is not empty');
              // tslint:disable-next-line: no-string-literal
              // tslint:disable-next-line: no-string-literal
              this.products.push({success : res['success'], data : res['data'], message : res['message'], code: res['code']});
              await this.modalController.dismiss(this.products);
            }else{
              console.log('data is empty');
              // tslint:disable-next-line: no-string-literal
              alert(res['message']);
            }
          }else{
            alert(res['message']);
          }
        });
    }else{
      alert('Please input the UPC#');
    }
  }

  async getStorage(key): Promise<any> {
    try {
        const result =  await this.storage.get(key);
        return result;
    }
    catch (e) { console.log(e); }
  }

}
