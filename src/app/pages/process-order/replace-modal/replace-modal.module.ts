import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReplaceModalPageRoutingModule } from './replace-modal-routing.module';

import { ReplaceModalPage } from './replace-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReplaceModalPageRoutingModule
  ],
  declarations: [ReplaceModalPage]
})
export class ReplaceModalPageModule {}
