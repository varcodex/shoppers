import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuantityModalPage } from './quantity-modal.page';

const routes: Routes = [
  {
    path: '',
    component: QuantityModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuantityModalPageRoutingModule {}
