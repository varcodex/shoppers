import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QuantityModalPage } from './quantity-modal.page';

describe('QuantityModalPage', () => {
  let component: QuantityModalPage;
  let fixture: ComponentFixture<QuantityModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuantityModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QuantityModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
