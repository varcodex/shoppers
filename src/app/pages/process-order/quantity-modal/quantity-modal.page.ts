import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { OrderService } from '../../../services/order.service';
import { Storage } from '@ionic/storage';
import { AutoLogoutService } from '../../../services/auto-logout.service';
@Component({
  selector: 'app-quantity-modal',
  templateUrl: './quantity-modal.page.html',
  styleUrls: ['./quantity-modal.page.scss'],
})
export class QuantityModalPage implements OnInit {
  quantity: number;
  sku: string;
  upc: any;
  orderId: any;
  token: any;
  constructor(private modalController: ModalController,
    private navParams: NavParams,
    private orderService: OrderService,
    private storage: Storage,
    private autoLogout: AutoLogoutService) { }

  async ngOnInit() {
    this.token = await this.getStorage('token');
    this.quantity = this.navParams.data.quantity;
    this.upc = this.navParams.data.upc;
    this.orderId = this.navParams.data.orderId;
    this.sku = this.navParams.data.sku;
  }

  async closeModal() {
    await this.modalController.dismiss(null);
  }

  async doUpdate(qty) {
    if(qty){
      if(this.quantity == qty){
        await this.modalController.dismiss(null);
      }else if(this.quantity < qty){
        await this.modalController.dismiss(null);
      }else if(qty == '0'){
        await this.modalController.dismiss(null);
      }else{
        const data: string = qty;
        let params = {
          order_id: this.orderId,
          upc: this.upc,
          quantity: qty
        };
        console.log(params)
        await this.orderService.updateOrderProduct(this.token, params).subscribe(async res => {
          if(res['success']){
            await this.modalController.dismiss(data);
            console.log('Order update successfully');
          }
        });
      }
    }else{
      alert('Please enter valid quantity!')
    }
  }

  async getStorage(key): Promise<any> {
    try {
        const result =  await this.storage.get(key);
        return result;
    }
    catch(e) { console.log(e) }
  }

}
