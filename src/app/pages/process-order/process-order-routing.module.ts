import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProcessOrderPage } from './process-order.page';

const routes: Routes = [
  {
    path: '',
    component: ProcessOrderPage
  },
  {
    path: 'products/:id',
    loadChildren: () => import('./products/products.module').then( m => m.ProductsPageModule)
  },
  {
    path: 'quantity-modal',
    loadChildren: () => import('./quantity-modal/quantity-modal.module').then( m => m.QuantityModalPageModule)
  },
  {
    path: 'replace-modal',
    loadChildren: () => import('./replace-modal/replace-modal.module').then( m => m.ReplaceModalPageModule)
  },
  {
    path: 'search-modal',
    loadChildren: () => import('./search-modal/search-modal.module').then( m => m.SearchModalPageModule)
  },
  {
    path: 'image-modal',
    loadChildren: () => import('./image-modal/image-modal.module').then( m => m.ImageModalPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProcessOrderPageRoutingModule {}
