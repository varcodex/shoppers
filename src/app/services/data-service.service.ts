import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
  private token = new BehaviorSubject<string>(null);
  private order = new BehaviorSubject<string[]>(null);
  private shoppersName = new BehaviorSubject<string>(null);
  private shoppersId = new BehaviorSubject<string>(null);
  private newOrderId = new BehaviorSubject<string>(null);
  private newOrderCount = new BehaviorSubject<string>(null);
  private jobOrderNo = new BehaviorSubject<string>(null);
  private areaOfDelivery = new BehaviorSubject<string>(null);
  private deliveryDate = new BehaviorSubject<string>(null);
  private OrderStatusId = new BehaviorSubject<string>(null);
  private CustomerName = new BehaviorSubject<string>(null);
  private membershipNumber = new BehaviorSubject<string>(null);
  private preauth = new BehaviorSubject<string>(null);

  castMembershipNumber = this.membershipNumber.asObservable();
  castpreauth = this.preauth.asObservable();
  castOrder = this.order.asObservable();
  castCustomerName = this.CustomerName.asObservable();
  castToken = this.token.asObservable();
  castShoppersname = this.shoppersName.asObservable();
  castshoppersId = this.shoppersId.asObservable();
  castnewOrderId = this.newOrderId.asObservable();
  castNewOrderCount = this.newOrderCount.asObservable();
  castjobOrderNo = this.jobOrderNo.asObservable();
  castareaOfDelivery = this.areaOfDelivery.asObservable();
  castdeliveryDate = this.deliveryDate.asObservable();
  castOrderStatusId = this.OrderStatusId.asObservable();
  constructor(private storage: Storage) { }

  setpreauth(preauth){
    this.storage.set('preauth', preauth);
    this.preauth.next(preauth);
  }

  setMembershipNumber(membershipNumber){
    this.storage.set('membershipNumber', membershipNumber);
    this.membershipNumber.next(membershipNumber);
  }

  setOrder(order){
    this.storage.set('order', order);
    this.order.next(order);
  }

  setjobOrderNo(jobOrderNo){
    this.storage.set('jobOrderNo', jobOrderNo);
    this.jobOrderNo.next(jobOrderNo);
  }
  setOrderStatusId(OrderStatusId){
    this.storage.set('OrderStatusId', OrderStatusId);
    this.OrderStatusId.next(OrderStatusId);
  }
  setareaOfDelivery(areaOfDelivery){
    this.storage.set('areaOfDelivery', areaOfDelivery);
    this.areaOfDelivery.next(areaOfDelivery);
  }
  setdeliveryDate(deliveryDate){
    this.storage.set('deliveryDate', deliveryDate);
    this.deliveryDate.next(deliveryDate);
  }
  setCustomerName(CustomerName){
    this.storage.set('CustomerName', CustomerName);
    this.CustomerName.next(CustomerName);
  }
  setToken(token){
    this.storage.set('token', token);
    this.token.next(token);
  }
  setshoppersName(shoppersName){
    this.storage.set('shoppersName', shoppersName);
    this.shoppersName.next(shoppersName);
  }
  setshoppersId(shoppersId){
    this.storage.set('shoppersId', shoppersId);
    this.shoppersId.next(shoppersId);}

  setnewOrderId(newOrderId){
    this.storage.set('newOrderId', newOrderId);
    this.newOrderId.next(newOrderId);
  }
  setnewOrderCount(newOrderCount){
    this.storage.set('newOrderCount', newOrderCount);
    this.newOrderCount.next(newOrderCount);
  }
  getpreauth(){
    this.storage.get('preauth').then((data) => {
      this.preauth.next(data);
    });
  }
  getMembershipNumber(){
    this.storage.get('membershipNumber').then((data) => {
      this.membershipNumber.next(data);
    });
  }
  getOrder(){
    this.storage.get('order').then((data) => {
      this.order.next(data);
    });
  }
  getOrderStatusId(){
    this.storage.get('OrderStatusId').then((data) => {
      this.OrderStatusId.next(data);
    });
  }
  getjobOrderNo(){
    this.storage.get('jobOrderNo').then((data) => {
      this.jobOrderNo.next(data);
    });
  }
  getareaOfDelivery(){
    this.storage.get('areaOfDelivery').then((data) => {
      this.areaOfDelivery.next(data);
    });
  }
  getdeliveryDate(){
    this.storage.get('deliveryDate').then((data) => {
      this.deliveryDate.next(data);
    });
  }
  getCustomerName(){
    this.storage.get('CustomerName').then((data) => {
      this.CustomerName.next(data);
    });
  }
  getToken(){
    this.storage.get('token').then((data) => {
      this.token.next(data);
    });
  }
  getshoppersName(){
    this.storage.get('shoppersName').then((data) => {
      this.shoppersName.next(data);
    });
  }
  getshoppersId(){
    this.storage.get('shoppersId').then((data) => {
      this.shoppersId.next(data);
    });
  }
  getnewOrderId(){
    this.storage.get('newOrderId').then((data) => {
      this.newOrderId.next(data);
    });
  }
  getnewOrderCount(){
    this.storage.get('newOrderCount').then((data) => {
      this.newOrderCount.next(data);
    });
  }
}
