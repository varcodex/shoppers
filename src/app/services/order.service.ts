import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { retry, catchError } from 'rxjs/operators';
import { Order } from '../models/order';
import { Capture } from '../models/capture';
import { Auth } from '../models/snr/auth';
import { Inventory } from '../models/snr/inventory';
import { Product } from '../models/product';
import { OrderStatus } from '../models/order-status';
import { DataServiceService } from './data-service.service';
@Injectable({
  providedIn: 'root'
})
export class OrderService {
  public headers: Headers;
  private token;
  constructor(private http: HttpClient,
              private dataServiceService: DataServiceService) {
        this.dataServiceService.castToken.subscribe(data => this.token = data);
   }

   capturerOrders(token, params): Observable<Capture[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    
    if(environment.type == '1'){ //shopper
      return this.http.post<Capture[]>(`${environment.api.s_url}/capture/payment`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Capture[]>(`${environment.api.p_url}/capture/payment`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
  }

  capturerPriceAdjustment(token, params): Observable<Capture[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    
      return this.http.post<Capture[]>(`${environment.api.urlAdjustments}/price-adjustments`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
  }

  capturerPayment(token, params): Observable<Capture[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    
      return this.http.post<Capture[]>(`${environment.api.urlAdjustments}/capture/payment`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
  }

  getAvailableNewOrders(token): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };

    if(environment.type == '1'){ //shopper
      return this.http.post<Order[]>(`${environment.api.s_url}/get/neworders`, '', httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Order[]>(`${environment.api.p_url}/get/neworders`, '', httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
    
  }

  getAvailableOnProcessOrders(token, params): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    if(environment.type == '1'){ //shopper
      return this.http.post<Order[]>(`${environment.api.s_url}/get/orders/onprocess`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Order[]>(`${environment.api.p_url}/get/orders/onprocess`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
  }

  getAvailableOrders(token, params): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    if(environment.type == '1'){ //shopper
      return this.http.post<Order[]>(`${environment.api.s_url}/get/ordersListByStatus`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Order[]>(`${environment.api.p_url}/get/ordersListByStatus`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
    
  }

  getAvailableOnPOSOrders(token, params): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    if(environment.type == '1'){ //shopper
      return this.http.post<Order[]>(`${environment.api.s_url}/get/orders/onpos`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Order[]>(`${environment.api.p_url}/get/orders/onpos`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
  }

  getAvailableOnForDeliveryOrders(token, params): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    if(environment.type == '1'){ //shopper
      return this.http.post<Order[]>(`${environment.api.s_url}/get/orders/delivery`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Order[]>(`${environment.api.p_url}/get/orders/delivery`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
  }

  getOrderStatusList(token): Observable<OrderStatus[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    if(environment.type == '1'){ //shopper
      return this.http.post<OrderStatus[]>(`${environment.api.s_url}/get/order/status/list`, '', httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<OrderStatus[]>(`${environment.api.p_url}/get/order/status/list`, '', httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
    
  }

  getOrderProducts(token, params): Observable<Product[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    if(environment.type == '1'){ //shopper
      return this.http.post<Product[]>(`${environment.api.s_url}/get/order/products`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Product[]>(`${environment.api.p_url}/get/order/products`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
    
  }

  getAccessToken(): Observable<Auth[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        // tslint:disable-next-line: object-literal-key-quotes
      })
    };

    let body = `username=snr_ecom&password=$NR3C0M&grant_type=password`;
    return this.http.post<Auth[]>(`${environment.api.snr}/Authenticate`, body, httpOptions).pipe(
      retry(0),
    );
  }

  getSKULocation(token, params): Observable<Inventory[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    return this.http.post<Inventory[]>(`${environment.api.snr}/api/ProductInventories/Find`, JSON.stringify([params]), httpOptions).pipe(
      retry(0),
    );
  }

  updateOrder(token, params): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    if(environment.type == '1'){ //shopper
      return this.http.post<Order[]>(`${environment.api.s_url}/update/order`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Order[]>(`${environment.api.p_url}/update/order`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
    
  }

  updateWeighted(token, params): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };

    if(environment.type == '1'){ //shopper
      return this.http.post<Order[]>(`${environment.api.s_url}/update/order/weighted/product`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      
    return this.http.post<Order[]>(`${environment.api.p_url}/update/order/weighted/product`, JSON.stringify(params), httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
    }
  }

  replaceOrder(token, params): Observable<Product[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };

    if(environment.type == '1'){ //shopper
      return this.http.post<Product[]>(`${environment.api.s_url}/replace/order/product`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Product[]>(`${environment.api.p_url}/replace/order/product`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
  }

  updateOrderProduct(token, params): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };

    if(environment.type == '1'){ //shopper
      return this.http.post<Order[]>(`${environment.api.s_url}/update/order/product`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Order[]>(`${environment.api.p_url}/update/order/product`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
  }

  updateProductStock(token, params): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    if(environment.type == '1'){ //shopper
      return this.http.post<Order[]>(`${environment.api.s_url}/update/order/oos`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Order[]>(`${environment.api.p_url}/update/order/oos`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
  }

  deleteWeightedProductStock(token, params): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': `Bearer ${token}`
      })
    };
    if(environment.type == '1'){ //shopper
      return this.http.post<Order[]>(`${environment.api.s_url}/delete/order/weighted/product`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<Order[]>(`${environment.api.p_url}/delete/order/weighted/product`, JSON.stringify(params), httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
  }

  errorHandl(error) {
    let errorMessage = '';
    // tslint:disable-next-line: whitespace
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    alert(errorMessage);
    return throwError(errorMessage);
  }
}
