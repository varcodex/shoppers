import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { retry, catchError } from 'rxjs/operators';
import { MyAccount } from '../models/my-account';


@Injectable({
  providedIn: 'root'
})
export class MyAccountService {

  public headers:Headers;
  constructor(private http: HttpClient) { }

  getDetails(token): Observable<MyAccount[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Accept': 'application/json, text/plain',
        'Authorization': `Bearer ${token}`
      })
    }
    if(environment.type == '1'){ //shopper
      return this.http.post<MyAccount[]>(`${environment.api.s_url}/details`, '', httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }else if(environment.type == '2'){ //picker
      return this.http.post<MyAccount[]>(`${environment.api.s_url}/details`, '', httpOptions).pipe(
        retry(0),
        catchError(this.errorHandl)
      );
    }
  }

  errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    alert(errorMessage);
    return throwError(errorMessage);
  }
}
