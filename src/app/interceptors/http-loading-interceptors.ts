import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { EMPTY, Observable, throwError } from 'rxjs';
import { LoadingController } from '@ionic/angular';
import { catchError, delay, finalize, map, retryWhen, take, tap } from 'rxjs/operators';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor{

    constructor(private loadingctrl: LoadingController){}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        this.loadingctrl.getTop().then(hasLoading => {
            if (!hasLoading){
                this.loadingctrl.create({
                    spinner: 'circular',
                    translucent: true
                }).then(loading => loading.present());
            }
        });

        return next.handle(request).pipe(
            catchError(err => {
                if (err instanceof HttpErrorResponse) {
                    switch ((<HttpErrorResponse>err).status) {
                        case 403:
                            console.log((<HttpErrorResponse>err).message);
                        // tslint:disable-next-line: no-switch-case-fall-through
                        default:
                            return throwError(err);
                    }
                } else {
                    return throwError(err);
                }
            }),
            retryWhen(err => {
                let retries = 1;
                return err.pipe(
                    delay(1000),
                    tap(() => {
                        console.log('retry');
                    }),
                    map(error => {
                        if (retries++ === 1){
                            throw error;
                        }
                        return error;
                    })
                );
            }),
            catchError(err => {
                let msg;
                if(err.error.code == '403'){ 
                    msg = err.error.message.error;
                    console.log(err)
                    alert(`Error Code(${err.error.code}): ${msg}`);
                }else if(err.error.code == '500'){
                    msg = err.error.message;
                    console.log(err)
                    alert(`Error Code(${err.error.code}): ${msg}`);
                }
                console.log(err);
                return EMPTY;
            }),
            finalize(async () => {
                let topLoader = await this.loadingctrl.getTop();
                await this.loadingctrl.dismiss();
                while (topLoader) {
                    if (!(await topLoader.dismiss())) {
                    throw new Error('Could not dismiss the topmost loader. Aborting...');
                    }
                    topLoader = await this.loadingctrl.getTop();
                }
            })
        );
    }
}
