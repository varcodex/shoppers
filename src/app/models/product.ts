export interface Product {
    success: boolean;
    data: [{
        id: number,
        product_id: number,
        cart_id: string,
        quantity: string,
        is_same_prod_diff_size: number,
        is_same_size_diff_brand: number,
        is_no_substitution: number,
        is_out_of_stock: number,
        scanned_upc: string,
        is_dc_delivery: string;
        dc_schedule: string;
        schedule1: number,
        schedule2: number,
        is_replaced: number,
        deleted_at: string,
        price_history: string,
        inventory: {
            id: number,
            sku: string,
            department_id: string,
            unit_price: string,
            store: string,
            sku_location: string,
            products: {
                id: string,
                sku: string,
                name: string,
                description: string,
                features: string[],
                featured_image: string[],
                feature_images_thumbnails: string[],
                sku_location: string,
                sku_main: string,
                product_type_id: string,
                upc: [{
                    id: string;
                    sku: string;
                    upc: string;
                }]
            }
        },
        sub_products: [
            {
                id: number;
                cart_products_id: string;
                scanned_upc: string;
                weight: string;
                price: string;
                deleted_at: string;
                created_at: string;
                updated_at: string;
            }]
    }];
    message: string;
    code: string;
}
