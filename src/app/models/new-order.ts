export interface NewOrder {
    id: number,
    shoppers_id : number,
    order_status_id : number,
    job_order_number: string,   
}
