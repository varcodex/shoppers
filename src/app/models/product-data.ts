export interface ProductData {
    quantity: string;
    is_same_prod_diff_size: number;
    is_same_size_diff_brand: number;
    is_no_substitution: number;
    is_out_of_stock: number;
    is_dc_delivery: string;
    delivery_date_time: string;
    id: number;
    product_id: number;
    sku: string;
    department_id: string;
    sku_location: string;
    name: string;
    featured_image: string;
    product_type_id: string,
    sub_product_id: number,
    upc: string;
    price_history: string;
    weight: string;
    color: string;
    isEdited: number;
    isReplaced: number;
    status: number;
}
