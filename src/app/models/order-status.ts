export interface OrderStatus {
    success: boolean;
    data:[{
        id: number,
        name : string,
        description :  string,
        deleted_at : string,
        created_at :  number,
        updated_at : number,
    }];
    message: string;
    code: string;
}
