export interface Inventory {
    sku: number,
    location: string,
    pickingSequence: number
}
