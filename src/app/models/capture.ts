export interface Capture {
    code: number;
    captured: boolean;
    message: string
}
