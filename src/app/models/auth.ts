export interface Auth {
    success: boolean;
    data:{
        token: string;
        name: string;
        code: string;
        id: number;
        logout: string;
        order: {
            id: number,
            shoppers_id : number,
            order_status_id : number,
            job_order_number: string,
        }
    };
    message: string;
}