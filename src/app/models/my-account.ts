export interface MyAccount {
    success:{
        id: string;
        name: string;
        email: string;
        role: number;
        username: string;
        employee_id: string;
        firstname: string;
        middlename: string;
        lastname: string;
        mobile_no: string;
        created_at: string;
    };
}
